# Scandoku

Status: early development

This project was born to create a sudoku GUI that had exactly the feature I wanted. I've tried a lot of players, and none of them were in the Goldilocks zone for me.

Eventually I hope to add machine vision to be able to read in existing sudoku puzzles using either a phone's camera or uploading an image. This is where the name comes from!

## Plan

☒ initial grid and cell navigation with mouse

☒ big number entry

☒ small number (candidate) entry

☒ keyboard navigation using arrow keys and numpad

☒ highlighting cell / row / col / box for selected and similar numbers

☒ col / row / box tallies

☒ multiple cell selection and candidate entry

☒ keyboard control for entry type

☒ keyboard control for mouse control style

☒ extra highlighting for single / binary values

☒ fill candidates logic

☒ error highlighting

☒ long form puzzle for id and url (81 char string)

☒ short form puzzle for url sharing (using byte64 type encoding)

☒ solver

☒ solver tests

☐ smart solver (using human logic)

☐ solution step output

☐ puzzle grader

☐ puzzle generation

☐ interface refinement

☐ ability to create and share puzzles

☐ ability to store puzzle history

☐ add timer

☐ ???

☐ add machine vision scanning

## Solver notes

Significant effort is going towards solver efficiency and solve time. In this effort, multiple solver algorithms have been attempted. Eventually instead of using numeric form, I switched to US city names in alphabetical order. Each solver will try every possibility and with return 'multiple solutions' if more than one exists, or 'no solution' if none exist. Most have basic validity built in and will return 'invalid' if that is the case.

### Atlanta

Very basic brute force method. Based on recursion using every combination. Nice and quick only for mostly solved puzzles. More open puzzes take tens of seconds. Some examples never stopped running.

### Boston

Variation of Atlanta that focused on tracking arrays of solved vs unsolved cells. Efficiency was not really better than Atlanta.

### Chicago

First solver with better efficiency. Kept a record of cell states and candidates that was available to all levels of recursion. The easiest cells to solve (naked singles) are done in separate stage than the forking. Forking involved keeping a backup state of peer candidates in local memory that was restored before trying each fork. 

This format was the first open to adding actual logic to reduce step count. Early drafts got the 'tens of seconds' puzzle solve to less than 2 seconds. With refinements it dropped to the 500ms range. It was the first solver to integrate statistics using `performance.now()`.

### Detroit

Failed attempt to move to a while loop instead of recursion.

### Eastlake

Stronger move towards reliance on human logic. A couple of methods were built out but the architecture was lacking. Puzzle state is passed through the recursive function, using a deep copy when forks occurred. 

Showed a lot of promise except for the ugly architecture. Maybe I should have saved the name 'Detroit' for this one?

### Fresno

My current solver. This one shows a lot of promise, with heavily abstracted stages and utility functions. The 'tens of seconds' puzzle from early on is in the 'tens of milliseconds' now. The solve time for simple puzzles can be a lot faster when only a few of the basic logic elements are employed. Turning all logic elements on makes this more optimized for harder puzzles.

I will be building on this one for the foreseeable future. I might one day move to bitwise functions to keep track of candidates (essentially, instead of using 1,2,3,4,5,6,7,8,9 as the sudoku elements, using 1,2,4,8,16,32,64,128,256,512 instead). I'm still about two orders of magnitude slower than the fastest Node.js solver I could find online.
