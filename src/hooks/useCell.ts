import { useEffect, useState } from 'react';
import { usePuzzleState, usePuzzleDispatch } from '@context';
import { EmptyCell, cellArrayIndex } from '@types';

const useCell = (row: number, col: number) => {
  const puzzle = usePuzzleState();
  const dispatch = usePuzzleDispatch();
  const [cell, setCell] = useState(EmptyCell);

  useEffect(() => {
    if (puzzle !== undefined) {
      const n = cellArrayIndex(row, col);
      if (puzzle?.cells && puzzle.cells.length >= n-1) {
        setCell(puzzle.cells[n]);
      } else {
        setCell(EmptyCell);
      }
    }
  }, [puzzle, row, col]);

  const toggleCellPencil = (n: number) => {
    dispatch({
      type: 'toggleCellPencil',
      cell: { row, col },
      payload: n,
    });
  };

  const setCellValue = (n: number) => {
    dispatch({
      type: 'setCellValue',
      cell: { row, col },
      payload: n,
    });
  };

  const unsetCellValue = () => {
    dispatch({
      type: 'unsetCellValue',
      cell: { row, col },
      payload: 0,
    });
  };

  const toggleSelectCell = () => {
    dispatch({
      type: 'toggleSelectCell',
      cell: { row, col },
      payload: 0,
    });
  };

  const toggleMultiSelectCell = () => {
    dispatch({
      type: 'toggleMultiSelectCell',
      cell: { row, col },
      payload: 0,
    });
  };

  const addMultiSelectCell = () => {
    dispatch({
      type: 'addMultiSelectCell',
      cell: { row, col },
      payload: 0,
    });
  };

  return {
    cell,
    toggleCellPencil,
    setCellValue,
    unsetCellValue,
    toggleSelectCell,
    toggleMultiSelectCell,
    addMultiSelectCell,
  };
};

export default useCell;

