import { useEffect, useState } from 'react';
import { usePuzzleState } from '@context';

const useRemaining = () => {
  const puzzle = usePuzzleState();
  const [ numsLeft, setNumsLeft ] = useState<number[]>([9,9,9,9,9,9,9,9,9]);

  useEffect(() => {
    const currentNums: number[] = [9,9,9,9,9,9,9,9,9];
    puzzle.cells.forEach(cell => {
      if (cell?.value) {
        currentNums[cell.value-1] -= 1;
      }
    });
    setNumsLeft(currentNums);
  }, [puzzle]);

  return {
    numsLeft,
  };
};

export default useRemaining;
