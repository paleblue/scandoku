import { useEffect, useState } from 'react';
import { usePuzzleState } from '@context';
import { CellLocation, Highlights, EmptyHighlights, NullLocation, isNullLocation, cellArrayIndex, cellLocationFromIndex, cellBox, cellBoxByIndex } from '@types';

type NumberInNumbers = Array<Array<number>>;
type ValuesExists = {
  rows: NumberInNumbers,
  columns: NumberInNumbers,
  boxes: NumberInNumbers,
};

const useHighlights = () => {
  const puzzle = usePuzzleState();
  const [highlights, setHighlights] = useState<Highlights>(EmptyHighlights);
  const [valuesExist, setValuesExist] = useState<ValuesExists>({ rows: [], columns: [], boxes: [] });
  const [selectedValue, setSelectedValue] = useState<number | undefined>(undefined);

  useEffect(() => {
    if (!isNullLocation(puzzle.selectedCell)) {
      const n = cellArrayIndex(puzzle.selectedCell as CellLocation);
      const v = puzzle.selectedValue || puzzle.cells[n].value;
      const matchCells: CellLocation[] = [];
      puzzle.cells.forEach((c, cIndex) => {
        if (c.value === v && cIndex !== n) {
          matchCells.push(cellLocationFromIndex(cIndex));
        }
      });

      setSelectedValue(v);
      if (v) {
        setHighlights((h) => ({
          ...h,
          valueHighlight: v,
          primaryHighlight: puzzle.cells[cellArrayIndex(puzzle.selectedCell || NullLocation)]?.value ? puzzle.selectedCell : undefined,
          secondaryHighlights: matchCells,
          multiSelectHighlights: puzzle.multiSelectCells || [],
        }));
      } else {
        setHighlights({
          ...EmptyHighlights,
          primaryHighlight: puzzle.selectedCell,
          multiSelectHighlights: puzzle.multiSelectCells || [],
        });
      }
    } else if (puzzle.selectedValue) {
      const matchCells: CellLocation[] = [];
      puzzle.cells.forEach((c, cIndex) => {
        if (c.value === puzzle.selectedValue) {
          matchCells.push(cellLocationFromIndex(cIndex));
        }
      });
      setHighlights((h) => ({
        ...h,
        valueHighlight: puzzle.selectedValue,
        primaryHighlight: undefined,
        secondaryHighlights: matchCells,
        multiSelectHighlights: puzzle.multiSelectCells || [],
      }));
    } else {
      setHighlights(EmptyHighlights);
    }
  }, [puzzle]);

  useEffect(() => {
    const vRows: NumberInNumbers = [[],[],[],[],[],[],[],[],[]];
    const vColumns: NumberInNumbers = [[],[],[],[],[],[],[],[],[]];
    const vBoxes: NumberInNumbers = [[],[],[],[],[],[],[],[],[]];
    puzzle.cells.forEach((c, cIndex) => {
      const loc = cellLocationFromIndex(cIndex);
      const boxnum = cellBoxByIndex(cIndex);
      if (c.value) {
        vRows[loc.row-1].push(c.value);
        vColumns[loc.col-1].push(c.value);
        vBoxes[boxnum-1].push(c.value);
      }
    });
    setValuesExist({
      rows: vRows,
      columns: vColumns,
      boxes: vBoxes,
    });
  }, [puzzle]);

  const isPrimaryHighlight = (row: number, col: number): boolean => {
    return (!!highlights.primaryHighlight && 
            (highlights.primaryHighlight.row === row && highlights.primaryHighlight.col === col));
  };

  const isMultiSelectHighlight = (row: number, col: number): boolean => {
    return highlights.multiSelectHighlights.some((c) => {
      return c.row === row && c.col === col;
    });
  };

  const isPrimaryAreaHighlight = (row: number, col: number): boolean => {
    return (!!highlights.primaryHighlight && 
              (
                highlights.primaryHighlight.row === row || 
                highlights.primaryHighlight.col === col || 
                isPrimarySameBox(row, col)
              )
            );
  };

  const isPrimarySameBox = (row: number, col: number): boolean => {
    return (!!highlights.primaryHighlight &&
              (
                (Math.floor((highlights.primaryHighlight.row-1)/3) === Math.floor((row-1)/3)) &&
                (Math.floor((highlights.primaryHighlight.col-1)/3) === Math.floor((col-1)/3))
              )
            );
  };

  const isSecondaryHighlight = (row: number, col: number): boolean => {
    return highlights.secondaryHighlights.some((c) => {
      return c.row === row && c.col === col;
    });
  };

  const isSecondaryAreaHighlight = (row: number, col: number): boolean => {
    return highlights.secondaryHighlights.some((c) => {
      return c.row === row || c.col === col || isSecondarySameBox(row, col);
    });
  };

  const isSecondarySameBox = (row: number, col: number): boolean => {
    return highlights.secondaryHighlights.some((c) => {
      return (
        (Math.floor((c.row-1)/3) === Math.floor((row-1)/3)) &&
        (Math.floor((c.col-1)/3) === Math.floor((col-1)/3))
      )
    });
  };

  const existsForCell = (row: number, col: number, n: number):boolean => {
    const boxnum = cellBox(row, col);
    const rowsExists: boolean = (valuesExist.rows[row-1] || []).filter(v => v === n).length > 1;
    const colsExists: boolean = (valuesExist.columns[col-1] || []).filter(v => v === n).length > 1;
    const boxesExists: boolean = (valuesExist.boxes[boxnum-1] || []).filter(v => v === n).length > 1;
    return rowsExists || colsExists || boxesExists;
  };

  const existsForPencil = (row: number, col: number, n: number):boolean => {
    const boxnum = cellBox(row, col);
    const rowsExists: boolean = (valuesExist.rows[row-1] || []).filter(v => v === n).length > 0;
    const colsExists: boolean = (valuesExist.columns[col-1] || []).filter(v => v === n).length > 0;
    const boxesExists: boolean = (valuesExist.boxes[boxnum-1] || []).filter(v => v === n).length > 0;
    return rowsExists || colsExists || boxesExists;
  };

  return {
    highlights,
    selectedValue,
    isPrimaryHighlight,
    isMultiSelectHighlight,
    isPrimaryAreaHighlight,
    isPrimarySameBox,
    isSecondaryHighlight,
    isSecondaryAreaHighlight,
    isSecondarySameBox,
    existsForCell,
    existsForPencil,
  };
};

export default useHighlights;
