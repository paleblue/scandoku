import { useEffect, useState } from 'react';
import { usePuzzleState } from '@context';
import { CellLocation, cellArrayIndex } from '@types';
import { NINE } from '@constants';

const useAvailableSpots = () => {
  const puzzle = usePuzzleState();
  const [ availableInRows, setAvailableInRows ] = useState<number[]>([]);
  const [ availableInCols, setAvailableInCols ] = useState<number[]>([]);
  const [ availableInBoxes, setAvailableInBoxes ] = useState<number[]>([]);

  useEffect(() => {
    if (puzzle.selectedValue) {
      const inRows: number[] = [];
      NINE.forEach(r => {
        let count: number = 0;
        NINE.forEach(c => {
          const loc: CellLocation = { row: r, col: c };
          const cell = puzzle.cells[cellArrayIndex(loc)];
          if (!cell?.value && cell.pencilValues.includes(puzzle.selectedValue as number)) {
            count += 1;
          }
        });
        inRows[r-0] = count;
      });
      const inCols: number[] = [];
      NINE.forEach(c => {
        let count: number = 0;
        NINE.forEach(r => {
          const loc: CellLocation = { row: r, col: c };
          const cell = puzzle.cells[cellArrayIndex(loc)];
          if (!cell?.value && cell.pencilValues.includes(puzzle.selectedValue as number)) {
            count += 1;
          }
        });
        inCols[c-0] = count;
      });

      // box 1 == box0,0
      // box 2 == box0,1
      // box 6 == box1,2
      // etc
      const inBoxes: number[] = [];
      [0,1,2].forEach(boxRow => {
        // row offset is 0 (+1,+2,+3) for boxes 1-3
        // row offset is 6 (+7,+8,+9) for boxes 7-9 
        // etc
        const rowOff = boxRow * 3;
        [0,1,2].forEach(boxCol => {
          let count: number = 0;
          const colOff = boxCol * 3;
          [1,2,3].forEach(r => {
            [1,2,3].forEach(c => {
              // for boxRow=0, boxCol=0, r=1, c=1 --> row = 1, col = 1
              // for boxRow=1, boxCol=2, r=3, c=2 --> row = 6, col = 8
              // etc
              const loc: CellLocation = { row: rowOff + r, col: colOff + c };
              const cell = puzzle.cells[cellArrayIndex(loc)];
              if (!cell?.value && cell.pencilValues.includes(puzzle.selectedValue as number)) {
                count += 1;
              }
            });
          });
          inBoxes[(boxRow*3)+boxCol] = count;
        });
      });

      setAvailableInRows(inRows);
      setAvailableInCols(inCols);
      setAvailableInBoxes(inBoxes);
    } else {
      setAvailableInRows([]);
      setAvailableInCols([]);
      setAvailableInBoxes([]);
    }
  }, [puzzle]);

  return {
    availableInRows,
    availableInCols,
    availableInBoxes,
  };
};

export default useAvailableSpots;
