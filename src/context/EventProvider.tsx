import React, { FC, useEffect, useCallback, createContext, useContext, Reducer, useReducer } from 'react';
import { CellLocation, NullLocation, CellControlMode, CellClickMode, AdditionalHighlightMode } from '@types';
import { usePuzzleDispatch, usePuzzleState, usePlayConfigState, usePlayConfigDispatch } from '@context';

type MouseEventAction = {
  type: 'startDragging' | 'endDragging';
  event: MouseEvent;
};
type MouseEventStore = {
  isDragging: boolean;
  origin?: { x: number, y: number };
};
const DefaultMouseEventStore = {
  isDragging: false,
};
type MouseEventDispatch = (action: MouseEventAction) => void;

interface KeyEventHelpers {}

const KeyEventContext = createContext<KeyEventHelpers>({});

const MouseEventContext = createContext<MouseEventStore>(DefaultMouseEventStore);
const MouseEventDispatchContext = createContext<MouseEventDispatch>(() => {});

const useMouseEventState = (): MouseEventStore => {
  const context = useContext(MouseEventContext);
	if (context === undefined) {
		throw new Error('useMouseEventState must be used within an EventProvider');
	}
  return context;
};

const useMouseEventDispatch = (): MouseEventDispatch => {
  const context = useContext(MouseEventDispatchContext);
	if (context === undefined) {
		throw new Error('useMouseEventDispatch must be used within an EventProvider');
	}
  return context;
};

const mouseEventReducer: Reducer<MouseEventStore, MouseEventAction> = (state: MouseEventStore, action: MouseEventAction): MouseEventStore => {
  switch(action.type) {
    case 'startDragging': {
      return {
        ...state,
        isDragging: true,
        origin: { x: action.event.clientX, y: action.event.clientY },
      };
    }
    case 'endDragging': {
      return {
        ...state,
        isDragging: false,
        origin: undefined,
      };
    }
  }
  return state;
};

const EventProvider: FC = ({ children }) => {
  const puzzle = usePuzzleState();
  const dispatch = usePuzzleDispatch();
  const playConfig = usePlayConfigState();
  const playConfigDispatch = usePlayConfigDispatch();

  const [mouseEventState, setMouseEventState] = useReducer(mouseEventReducer, DefaultMouseEventStore);

  const setCellValue = useCallback((n: number) => {
    if (puzzle?.selectedCell) {
      const cell = puzzle.selectedCell;
      if ((puzzle.multiSelectCells || []).length > 0) {
        // multiselect always adds pencil marks
        dispatch({
          type: 'toggleMultiCellPencil',
          cell: cell,
          payload: n,
        });
      } else {
        switch(playConfig.cellControlMode) {
          case 'highlight': {
            dispatch({
              type: 'toggleSelectValue',
              cell: NullLocation,
              payload: n,
            });
            break;
          }
          case 'big': {
            dispatch({
              type: 'setCellValue',
              cell: cell,
              payload: n,
            });
            break;
          }
          case 'pencil': {
            dispatch({
              type: 'toggleCellPencil',
              cell: cell,
              payload: n,
            });
            break;
          }
        }
      }
    } else {
      dispatch({
        type: 'toggleSelectValue',
        cell: NullLocation,
        payload: n,
      });
    }
  }, [puzzle, dispatch, playConfig.cellControlMode]);

  const unsetCellValue = useCallback(() => {
    if (puzzle?.selectedCell) {
      dispatch({
        type: 'unsetCellValue',
        cell: puzzle.selectedCell,
        payload: 0,
      });
    }
  }, [puzzle,dispatch]);

  const setCellControlMode = useCallback((mode: CellControlMode) => {
    playConfigDispatch({
      type: 'cellControlMode',
      value: mode,
    });
  }, [playConfigDispatch]);

  const cycleCellControlMode = useCallback(() => {
    switch(playConfig.cellControlMode) {
      case 'highlight': {
        setCellControlMode('big');
        break;
      }
      case 'big': {
        setCellControlMode('pencil');
        break;
      }
      case 'pencil': {
        setCellControlMode('highlight');
        break;
      }
    }
  }, [playConfig.cellControlMode, setCellControlMode]);

  const setCellClickMode = useCallback((mode: CellClickMode) => {
    playConfigDispatch({
      type: 'cellClickMode',
      value: mode,
    });
  }, [playConfigDispatch]);
  
  const toggleCellClickMode = useCallback(() => {
    switch(playConfig.cellClickMode) {
      case 'pencil': {
        setCellClickMode('select');
        break;
      }
      case 'select': {
        setCellClickMode('pencil');
        break;
      }
    }
  }, [playConfig.cellClickMode, setCellClickMode]);

  const toggleCellPencilMode = useCallback(() => {
    if (puzzle.selectedCell) {
      dispatch({
        type: 'togglePencilMode',
        cell: puzzle.selectedCell,
        payload: 0,
      });
    }
  }, [puzzle, dispatch]);

  const setAdditionalHighlightMode = useCallback((mode: AdditionalHighlightMode) => {
    playConfigDispatch({
      type: 'additionalHighlightMode',
      value: mode,
    });
  }, [playConfigDispatch]);

  const cycleAdditionalHighlightMode = useCallback(() => {
    switch(playConfig.additionalHighlightMode) {
      case 'none': {
        setAdditionalHighlightMode('singles');
        break;
      }
      case 'singles': {
        setAdditionalHighlightMode('binary');
        break;
      }
      case 'binary': {
        setAdditionalHighlightMode('none');
        break;
      }
    }
  }, [playConfig.additionalHighlightMode, setAdditionalHighlightMode]);

  const selectCell = useCallback((loc?: CellLocation | 'up' | 'down' | 'left' | 'right' | 'upleft' | 'upright' | 'downleft' | 'downright') => {
    let newloc: CellLocation = (typeof loc === typeof NullLocation) ?  loc as CellLocation : (puzzle.selectedCell || puzzle.lastSelectedCell) as CellLocation;

    switch(loc) {
      case 'up':
        newloc = {
          ...newloc,
          row: (newloc.row > 1) ? newloc.row - 1 : 9,
        };
        break;
      case 'upleft':
        newloc = {
          col: (newloc.col > 1) ? newloc.col - 1 : 9,
          row: (newloc.row > 1) ? newloc.row - 1 : 9,
        };
        break;
      case 'upright':
        newloc = {
          col: (newloc.col < 9) ? newloc.col + 1 : 1,
          row: (newloc.row > 1) ? newloc.row - 1 : 9,
        };
        break;
      case 'down':
        newloc = {
          ...newloc,
          row: (newloc.row < 9) ? newloc.row + 1 : 1,
        };
        break;
      case 'downleft':
        newloc = {
          col: (newloc.col > 1) ? newloc.col - 1 : 9,
          row: (newloc.row < 9) ? newloc.row + 1 : 1,
        };
        break;
      case 'downright':
        newloc = {
          col: (newloc.col < 9) ? newloc.col + 1 : 1,
          row: (newloc.row < 9) ? newloc.row + 1 : 1,
        };
        break;
      case 'left':
        newloc = {
          ...newloc,
          col: (newloc.col > 1) ? newloc.col - 1 : 9,
        };
        break;
      case 'right':
        newloc = {
          ...newloc,
          col: (newloc.col < 9) ? newloc.col + 1 : 1,
        }
        break;
    }
    dispatch({
      type: 'toggleSelectCell',
      cell: newloc,
      payload: 0,
    });
  }, [puzzle,dispatch]);

  const undoPuzzleHistory = useCallback(() => {
    dispatch({
      type: 'undo',
      cell: NullLocation,
      payload: 0,
    });
  }, [dispatch]);

  const redoPuzzleHistory = useCallback(() => {
    dispatch({
      type: 'redo',
      cell: NullLocation,
      payload: 0,
    });
  }, [dispatch]);

  const keyDownHandler = useCallback((event: KeyboardEvent) => {
    // don't do anything if we're not on the puzzle page
    if (!document.querySelector('#puzzle')) return;

    // console.log('key event', event);
    if (puzzle?.selectedCell) {
      if (event.ctrlKey) {
      } else {
        if (['Backspace','Delete'].includes(event.key)) {
          event.preventDefault();
          unsetCellValue();
        } else if (event.key === 'ArrowUp' || event.key === 'k') {
          event.preventDefault();
          selectCell('up');
        } else if (event.key === 'ArrowDown' || event.key === 'j') {
          event.preventDefault();
          selectCell('down');
        } else if (event.key === 'ArrowLeft' || event.key === 'h') {
          event.preventDefault();
          selectCell('left');
        } else if (event.key === 'ArrowRight' || event.key === 'l') {
          event.preventDefault();
          selectCell('right');
        } else if (event.key === 'Home' && event.code === 'Numpad7') {
          event.preventDefault();
          selectCell('upleft');
        } else if (event.key === 'PageUp' && event.code === 'Numpad9') {
          event.preventDefault();
          selectCell('upright');
        } else if (event.key === 'End' && event.code === 'Numpad1') {
          event.preventDefault();
          selectCell('downleft');
        } else if (event.key === 'PageDown' && event.code === 'Numpad3') {
          event.preventDefault();
          selectCell('downright');
        }
      }
    } else {
      if (event.ctrlKey) {
      } else {
        if (event.key === 'ArrowUp' || event.key === 'ArrowDown' || event.key === 'ArrowLeft' || event.key === 'ArrowRight') {
          event.preventDefault();
          selectCell();
        }
      }
    }
    // for these events, it doesn't matter if there is a selected cell
    // events using ctrlKey first
    if (event.ctrlKey) {
      if (event.key === 'z') {
        event.preventDefault();
        undoPuzzleHistory();
      } else if (event.key === 'y') {
        event.preventDefault();
        redoPuzzleHistory();
      }
    } else {
      if (['1','2','3','4','5','6','7','8','9'].includes(event.key)) {
        // usually requires a selected cell, but if there is not one it will highlight the value
        event.preventDefault();
        setCellValue(parseInt(event.key));
      } else if (event.key === 'n') {
        event.preventDefault();
        undoPuzzleHistory();
      } else if (event.key === 'm') {
        event.preventDefault();
        redoPuzzleHistory();
      } else if (event.key === 'Tab') {
        event.preventDefault();
        cycleCellControlMode();
      } else if (event.key === 'a') {
        event.preventDefault();
        setCellControlMode('big');
      } else if (event.key === 's') {
        event.preventDefault();
        setCellControlMode('pencil');
      } else if (event.key === 'v') {
        event.preventDefault();
        toggleCellClickMode();
      } else if (event.key === 'd') {
        event.preventDefault();
        toggleCellPencilMode();
      } else if (event.key === 'b') {
        event.preventDefault();
        cycleAdditionalHighlightMode();
      } else if (event.key === 'f') {
        event.preventDefault();
        setCellControlMode('highlight');
      }
    }
  }, [puzzle, setCellValue, unsetCellValue, undoPuzzleHistory, redoPuzzleHistory, selectCell, cycleCellControlMode, setCellControlMode, toggleCellClickMode, toggleCellPencilMode, cycleAdditionalHighlightMode]);

  useEffect(() => {
    document.querySelector('body')?.addEventListener('keydown', keyDownHandler, false);
    return (() => {
      document.querySelector('body')?.removeEventListener('keydown', keyDownHandler);
    });
  }, [keyDownHandler]);

  return (
    <KeyEventContext.Provider value={{}}>
      <MouseEventContext.Provider value={mouseEventState}>
        <MouseEventDispatchContext.Provider value={setMouseEventState}>
          {children}
        </MouseEventDispatchContext.Provider>
      </MouseEventContext.Provider>
    </KeyEventContext.Provider>
  );
};

export { EventProvider, useMouseEventState, useMouseEventDispatch };
