import React, { Reducer, useReducer, useContext, createContext, useEffect } from 'react';
import { CellControlMode, CellClickMode, AdditionalHighlightMode, CELL_CONTROL_MODE, CELL_CLICK_MODE, ADDITIONAL_HIGHLIGHT_MODE, PlayConfig, DefaultPlayConfig } from '@types';

export type Action = {
  type: 'cellControlMode' | 'cellClickMode' | 'additionalHighlightMode';
  value: CellControlMode | CellClickMode | AdditionalHighlightMode;
};
export type Dispatch = (action: Action) => void;
type PlayConfigProviderProps = { children: React.ReactNode };

export const PlayConfigContext = createContext<PlayConfig | undefined>(undefined);
export const PlayConfigDispatchContext = createContext<Dispatch | undefined>(undefined);

const playConfigReducer: Reducer<PlayConfig, Action> = (state: PlayConfig, action: Action): PlayConfig => {
  switch(action.type) {
    case 'cellControlMode': {
      window.localStorage.setItem(CELL_CONTROL_MODE, action.value);
      return {
        ...state,
        cellControlMode: action.value as CellControlMode
      };
    }
    case 'cellClickMode': {
      window.localStorage.setItem(CELL_CLICK_MODE, action.value);
      return {
        ...state,
        cellClickMode: action.value as CellClickMode
      };
    }
    case 'additionalHighlightMode': {
      window.localStorage.setItem(ADDITIONAL_HIGHLIGHT_MODE, action.value);
      return {
        ...state,
        additionalHighlightMode: action.value as AdditionalHighlightMode
      };
    }
  }
  return state;
};

const PlayConfigProvider = ({ children }: PlayConfigProviderProps): JSX.Element => {
  const [playConfigState, setPlayConfigState] = useReducer(playConfigReducer, DefaultPlayConfig);

  useEffect(() => {
    // initial config load
    const loadedControlMode = window.localStorage.getItem(CELL_CONTROL_MODE) as CellControlMode;
    setPlayConfigState({
      type: 'cellControlMode',
      value: loadedControlMode || DefaultPlayConfig.cellControlMode,
    });
    const loadedClickMode = window.localStorage.getItem(CELL_CLICK_MODE) as CellClickMode;
    setPlayConfigState({
      type: 'cellClickMode',
      value: loadedClickMode || DefaultPlayConfig.cellClickMode,
    });
    const loadedAdditionalHighlightMode = window.localStorage.getItem(ADDITIONAL_HIGHLIGHT_MODE) as AdditionalHighlightMode;
    setPlayConfigState({
      type: 'additionalHighlightMode',
      value: loadedAdditionalHighlightMode || DefaultPlayConfig.additionalHighlightMode,
    });
  }, []);

  return (
    <PlayConfigContext.Provider value={playConfigState}>
      <PlayConfigDispatchContext.Provider value={setPlayConfigState}>
        {children}
      </PlayConfigDispatchContext.Provider>
    </PlayConfigContext.Provider>
  );
};

const usePlayConfigState = (): PlayConfig => {
  const context = useContext(PlayConfigContext);
	if (context === undefined) {
		throw new Error('usePlayConfigDispatch must be used within a PlayConfigProvider');
	}
  return context;
};

const usePlayConfigDispatch = (): Dispatch => {
  const context = useContext(PlayConfigDispatchContext);
	if (context === undefined) {
		throw new Error('usePlayConfigDispatch must be used within a PlayConfigProvider');
	}
  return context;
};

export { PlayConfigProvider, usePlayConfigState, usePlayConfigDispatch };
