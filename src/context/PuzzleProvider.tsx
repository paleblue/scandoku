import React, { Reducer, useReducer, useState, useEffect, useContext, createContext } from 'react';
import { Cell, cellArrayIndex, CellLocation, isNullLocation, Puzzle, EmptyPuzzle, cellBox, indexesOfRow, indexesOfColumn, indexesOfBox, cellLocationFromIndex } from '@types';
import { Solved } from '@components';

export type Action = {
	type: 'field' | 'setPuzzle' | 'setCell' | 'setCellValue' | 'unsetCellValue' | 'toggleCellPencil' | 'toggleSelectCell' | 'toggleMultiSelectCell' | 'addMultiSelectCell' | 'toggleSelectValue' | 'toggleMultiCellPencil' | 'undo' | 'redo' | 'resetPuzzle' | 'togglePencilMode' | 'fillCandidates';
	cell: CellLocation;
	payload: string | number | Cell | Puzzle;
};
export type Dispatch = (action: Action) => void;
type PuzzleProviderProps = { children: React.ReactNode };

const PuzzleStateContext = createContext<Puzzle | undefined>(undefined);
const PuzzleDispatchContext = createContext<Dispatch | undefined>(undefined);

type PuzzleHistory = {
  state: Puzzle,
  action: Action,
};

const puzzleHistory: PuzzleHistory[] = [];
let historyIndex: number = 0;

const saveToLocalStorage = (puzzle: Puzzle | undefined) => {
  if (puzzle) {
    const id: string = puzzle.id || Puzzle.toLongForm(puzzle);
    const pstr: string = JSON.stringify(puzzle);
    window.localStorage.setItem(id, pstr);
  }
};

const getFromLocalStorage = (id: string): Puzzle | undefined => {
  const storedInfo: string | null = window.localStorage.getItem(id);
  return storedInfo ? JSON.parse(storedInfo) as Puzzle : undefined;
};

const removeFromLocalStorage = (id: string) => {
  window.localStorage.removeItem(id);
};

const addToHistory = (state: Puzzle | undefined, action: Action) => {
  if (state) {
    // clear redos first
    puzzleHistory.length = historyIndex + 1;

    // now add state
    puzzleHistory.push({ state, action });
    saveToLocalStorage(state);
    historyIndex = puzzleHistory.length - 1;
  }
};

const clearHistory = () => {
  puzzleHistory.length = 0;
  historyIndex = -1;
};

const puzzleReducer: Reducer<Puzzle, Action> = (state: Puzzle, action: Action): Puzzle => {
	switch (action.type) {
    case 'setPuzzle': {
      clearHistory();
      if (typeof action.payload === typeof EmptyPuzzle) {
        const fromSavedState = getFromLocalStorage((action.payload as Puzzle).id);
        addToHistory(fromSavedState || action.payload as Puzzle, action);
        return action.payload as Puzzle;
      } else if (typeof action.payload === 'string') {
        const longForm = (action.payload as string).length === 81 ? action.payload as string : Puzzle.shortToLongForm(action.payload as string);
        const fromSavedState = getFromLocalStorage(longForm);
        if (fromSavedState) {
          addToHistory(fromSavedState as Puzzle, action);
          return fromSavedState as Puzzle;
        } else {
          const p = new Puzzle(action.payload as string);
          addToHistory(p, action);
          return p;
        }
      }
      return EmptyPuzzle;
    }
    case 'setCellValue': {
      const cell = state.cells[cellArrayIndex(action.cell)];

      if (cell.fixed) {
        return {
          ...state
        };
      }

      const { row: setRow, col: setCol } = action.cell;
      const iOfRow = indexesOfRow(setRow);
      const iOfCol = indexesOfColumn(setCol);
      const iOfBox = indexesOfBox(cellBox(setRow, setCol));
      const newState = {
        ...state,
        selectedValue: action.payload as number,
        cells: (state?.cells || []).map((n, i) => {
          const { row, col } = action.cell;
          const targetIndex = cellArrayIndex(row, col);
          if (i === targetIndex && n) {
            return {
              ...n,
              value: action.payload as number,
              pencilValues: n.pencilValues.filter(p => p !== action.payload as number),
            };
          } else if (n && !n.value && (iOfRow.includes(i) || iOfCol.includes(i) || iOfBox.includes(i))) {
            return {
              ...n,
              pencilValues: n.pencilValues.filter(p => p !== action.payload as number),
            };
          } else {
            return n;
          }
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
    case 'unsetCellValue': {
      const cell = state.cells[cellArrayIndex(action.cell)];

      if (cell.fixed) {
        return {
          ...state
        };
      }

      const newState = {
        ...state,
        selectedValue: undefined,
        selectedCell: undefined,
        cells: (state?.cells || []).map((n, i) => {
          const { row, col } = action.cell;
          const targetIndex = cellArrayIndex(row, col);
          if (i === targetIndex && n) {
            return {
              ...n,
              value: undefined,
            };
          } else {
            return n;
          }
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
    case 'toggleCellPencil': {
      const newState = {
        ...state,
        selectedValue: (state?.cells[cellArrayIndex(action.cell)].pencilValues.includes(action.payload as number) ? undefined : (action.payload as number)),
        cells: (state?.cells || []).map((n, i) => {
          const { row, col } = action.cell;
          const targetIndex = cellArrayIndex(row, col);
          if (i === targetIndex && n) {
            if (n.pencilValues.includes(action.payload as number)) {
              return {
                ...n,
                pencilValues: n.pencilValues.filter(n => n !== action.payload as number),
              };
            } else {
              return {
                ...n,
                pencilValues: [ ...(n.pencilValues), action.payload as number ],
              };
            }
          } else {
            return n;
          }
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
    case 'toggleMultiCellPencil': {
      if (!state.selectedCell && (state.multiSelectCells || []).length > 0) {
        return {
          ...state,
        };
      }

      const initialStateIsMarked: boolean = state.cells[cellArrayIndex(state.selectedCell as CellLocation)].pencilValues.includes(action.payload as number);
      const newState = {
        ...state,
        selectedValue: (initialStateIsMarked ? undefined : (action.payload as number)),
        cells: (state?.cells || []).map((n, i) => {
          const loc: CellLocation = cellLocationFromIndex(i);
          if ((loc.row === state.selectedCell?.row && loc.col === state.selectedCell?.col) || (state.multiSelectCells || []).some(m => m.row === loc.row && m.col === loc.col)) {
            if (initialStateIsMarked) {
              return {
                ...n,
                pencilValues: n.pencilValues.filter(n => n !== action.payload as number),
              };
            } else {
              return {
                ...n,
                pencilValues: [ ...(n.pencilValues), action.payload as number ],
              };
            }
          }
          return {
            ...n,
          };
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
    case 'toggleSelectCell': {
      const c: CellLocation | undefined = isNullLocation(action.cell) ? undefined : action.cell;
      if (!isNullLocation(c)) {
        if (c && state.selectedCell && !isNullLocation(state.selectedCell) && state.selectedCell.row === c.row && state.selectedCell.col === c.col) {
          return {
            ...state,
            selectedCell: undefined,
            lastSelectedCell: state.selectedCell,
            selectedValue: undefined,
            multiSelectCells: [],
          };
        } else if (c) {
          const n = state.cells[cellArrayIndex(c.row, c.col)]?.value;
          return {
            ...state,
            selectedCell: c,
            lastSelectedCell: !isNullLocation(state.selectedCell) ? state.selectedCell : state.lastSelectedCell,
            selectedValue: n,
            multiSelectCells: [],
          };
        } else {
          return {
            ...state,
            selectedCell: undefined,
            lastSelectedCell: !isNullLocation(state.selectedCell) ? state.selectedCell : state.lastSelectedCell,
            selectedValue: undefined,
            multiSelectCells: [],
          };
        }
      } else {
        // maybe it just loaded with no selected cells
        return {
          ...state,
          selectedCell: { row: 1, col: 1 },
          lastSelectedCell: { row: 1, col: 1 },
          selectedValue: state.cells[0].value,
          multiSelectCells: [],
        };
      }
    }
    case 'toggleMultiSelectCell': {
      const c: CellLocation | undefined = isNullLocation(action.cell) ? undefined : action.cell;
      // ignore if cell has a value
      if (!isNullLocation(c) && !state.cells[cellArrayIndex(c as CellLocation)].value) {
        if (c && (state.multiSelectCells || []).some(m => c.row === m.row && c.col === m.col)) {
          const limitedSelectCells: CellLocation[] = (state.multiSelectCells || []).filter(m => !(c.row === m.row && c.col === m.col));
          return {
            ...state,
            multiSelectCells: limitedSelectCells,
          };
        } else if (c) {
          return {
            ...state,
            selectedCell: state.selectedCell || c,
            multiSelectCells: [...(state.multiSelectCells || []), c],
          };
        }
      }
      return {
        ...state,
      };
    }
    case 'addMultiSelectCell': {
      const c: CellLocation | undefined = isNullLocation(action.cell) ? undefined : action.cell;
      // ignore if cell has a value
      if (!isNullLocation(c) && !state.cells[cellArrayIndex(c as CellLocation)].value) {
        // or if it is added already, or it's for the existing cell
        if (c && state.selectedCell &&
            (state.selectedCell.row !== c.row || state.selectedCell.col !== c.col) &&
            (state.multiSelectCells || []).some(m => c.row === m.row && c.col === m.col) === false) {
          return {
            ...state,
            multiSelectCells: [...(state.multiSelectCells || []), c],
          };
        }
      }
      return {
        ...state,
      };
    }
    case 'toggleSelectValue': {
      if (state.selectedValue === action.payload ) {
        return {
          ...state,
          selectedCell: undefined,
          lastSelectedCell: !isNullLocation(state.selectedCell) ? state.selectedCell : state.lastSelectedCell,
          selectedValue: undefined,
        };
      } else {
        return {
          ...state,
          selectedCell: undefined,
          lastSelectedCell: !isNullLocation(state.selectedCell) ? state.selectedCell : state.lastSelectedCell,
          selectedValue: action.payload as number,
        };
      }
    }
    case 'undo': {
      historyIndex = Math.max(0, historyIndex - 1);
      if (puzzleHistory[historyIndex]) {
        return {
          ...puzzleHistory[historyIndex].state,
        };
      } else {
        return {
          ...state,
        };
      }
    }
    case 'redo': {
      historyIndex = Math.min(historyIndex + 1, puzzleHistory.length - 1);
      if (puzzleHistory[historyIndex]) {
        return {
          ...puzzleHistory[historyIndex].state,
        };
      } else {
        return {
          ...state,
        };
      }
    }
    case 'resetPuzzle': {
      const longForm = state.id;
      clearHistory();
      removeFromLocalStorage(longForm);
      const p = Puzzle.parseString(longForm);
      addToHistory(p, action);
      return p;
    }
    case 'togglePencilMode': {
      const newState: Puzzle = {
        ...state,
        cells: state.cells.map((c, i) => {
          const loc = cellLocationFromIndex(i);
          if (state.selectedCell) {
            if ((loc.row === state.selectedCell.row && loc.col === state.selectedCell.col) || (state.multiSelectCells || []).some(m => m.row === loc.row && m.col === loc.col)) {
              const orig = state.cells[cellArrayIndex(state.selectedCell)]?.pencilMode || 'corners';
              const newPencilMode = orig === 'center' ? 'corners' : 'center';
              return {
                ...c,
                pencilMode: newPencilMode,
              };
            }
          }
          return c;
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
    case 'fillCandidates': {
      const newState: Puzzle = {
        ...state,
        cells: state.cells.map((c, i) => {
          if (c.value) {
            return c;
          } else {
            const loc = cellLocationFromIndex(i);
            return {
              ...c,
              pencilValues: Puzzle.getCandidates(state, loc.row, loc.col),
            };
          }
        }),
      };
      addToHistory(newState, action);
      return newState;
    }
		default: {
			throw new Error(`Unhandled action type: ${action.type}`);
		}
	}
}

const PuzzleProvider = ({ children }: PuzzleProviderProps): JSX.Element => {
	const [puzzleState, setPuzzleState] = useReducer(puzzleReducer, EmptyPuzzle);
  const [isSolved, setIsSolved] = useState(false);

  useEffect(() => {
    setIsSolved(Puzzle.isSolved(puzzleState));
  }, [puzzleState]);

	return (
		<PuzzleStateContext.Provider value={puzzleState}>
			<PuzzleDispatchContext.Provider value={setPuzzleState}>
        <div className="puzzle-context">
          {children}
          <Solved visible={isSolved} />
        </div>
			</PuzzleDispatchContext.Provider>
		</PuzzleStateContext.Provider>
	);
};
const usePuzzleState = (): Puzzle => {
	const context = useContext(PuzzleStateContext);
	if (context === undefined) {
		throw new Error('usePuzzleState must be used within a PuzzleProvider');
	}
	return context;
};

const usePuzzleDispatch = (): Dispatch => {
	const context = useContext(PuzzleDispatchContext);
	if (context === undefined) {
		throw new Error('usePuzzleDispatch must be used within a PuzzleProvider');
	}
	return context;
};
export { PuzzleProvider, usePuzzleState, usePuzzleDispatch };

