import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { PuzzleProvider, EventProvider, PlayConfigProvider } from '@context';
import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { DefaultTheme } from '@themes';
import '@scss/App.scss';

import { Puzzle, OpenNav } from '@containers';

function App() {
  return (
    <PlayConfigProvider>
      <PuzzleProvider>
        <EventProvider>
          <BrowserRouter>
            <ThemeProvider theme={DefaultTheme}>
              <CssBaseline />
              <div className="App">
                <Switch>
                  <Route path="/puzzle" exact component={Puzzle} />
                  <Route path="/puzzle/:puzzleDefinition" component={Puzzle} />
                  <Route component={OpenNav} />
                </Switch>
              </div>
            </ThemeProvider>
          </BrowserRouter>
        </EventProvider>
      </PuzzleProvider>
    </PlayConfigProvider>
  );
}

export default App;
