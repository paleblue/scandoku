import { iSolver } from '@solvers';
import { Puzzle, PuzzleCell, SolutionConfig, SolutionOutput, EmptySolutionOutput, SolutionConfig_NoLogging, indexesOfRow } from '@types';
import { BaseCells, NINE } from '@constants';

export class Chicago implements iSolver {
	public getSolution = async (puzzle: string, _solutionConfig: SolutionConfig = SolutionConfig_NoLogging ): Promise<SolutionOutput> => {
    type PeerCandidate = { index: number, peers: number[] };

    if (puzzle.length !== 81) return { ...EmptySolutionOutput, solution: 'invalid' };

    const cells = BaseCells.map((c, i) => {
      const v = parseInt(puzzle.substr(i, 1));
      return {
        ...c,
        value: v,
        fixed: v > 0,
      };
    });

    const rows: PuzzleCell[][] = NINE.map(n => cells.filter(c => c.row === n));
    const cols: PuzzleCell[][] = NINE.map(n => cells.filter(c => c.col === n));
    const boxes: PuzzleCell[][] = NINE.map(n => cells.filter(c => c.box === n));

    let candidates: number[] = [];
    let stepCount = 0;
    let numReduceCandidatesForPeers = 0;
    let numSetCandidatesForPeers = 0;
    let numGetCandidateIndexes = 0;
    let numRestoreCandidatePeers = 0;
    let timeReduceCandidatesForPeers = 0;
    let timeSetCandidatesForPeers = 0;
    let timeGetCandidateIndexes = 0;
    let timeRestoreCandidatePeers = 0;
    let maxTimesInRowFor: { [key: string]: number } = {};
    let curTimesInRowFor: { [key: string]: number } = {};
    let maxTimesCount: string = '';

    const reduceCandidatesForPeers = (index: number, cand: number): boolean => {
      numReduceCandidatesForPeers++;
      const startTime = performance.now();
      let passes: boolean = true;
      cells[index].peers.forEach(p => {
        if (cells[p].value === 0) {
          cells[p].candidates = cells[p].candidates.filter(f => f !== cand);
          passes = passes && cells[p].candidates.length > 0;
        }
      });
      const endTime = performance.now();
      timeReduceCandidatesForPeers += endTime - startTime;
      return passes; 
    };

    const setAllCandidates = () => {
      NINE.forEach(c => {
        indexesOfRow(c).forEach(i => {
          if (cells[i].value < 1) {
            cells[i].candidates = NINE.filter(n => (
              !rows[cells[i].row-1].map(r => r.value).includes(n) && 
              !cols[cells[i].col-1].map(r => r.value).includes(n) && 
              !boxes[cells[i].box-1].map(r => r.value).includes(n)
            ));
          } else {
            cells[i].candidates = [];
          }
        });
      });
    };

    const getCandidateIndexes = () => {
      numGetCandidateIndexes++;
      const startTime = performance.now();
      candidates = cells
        .filter(c => c.value === 0 && c.candidates.length > 0)
        .sort((a, b) => a.candidates.length - b.candidates.length)
        .map(c => c.index);
      const endTime = performance.now();
      timeGetCandidateIndexes += endTime - startTime;
    };

    const s_isFilled = (): boolean => {
      return cells.every(c => c.value > 0);
    };

    const s_isSolved = (): boolean => {
      return Puzzle.isSolved(cells.map(c => c.value).join(''));
    };

    const stage_NakedSingle = (firstCell: number): string[] => {
      let solutions: string[] = [];
      const nc = cells[firstCell].candidates[0];
      const startTimep1 = performance.now();
      const peerCandidates: PeerCandidate[] = cells[firstCell].peers.map(p => ({ index: p, peers: cells[p].candidates }));
      const endTimep1 = performance.now();
      if (maxTimesCount === 'naked single') {
        curTimesInRowFor['naked single'] += 1;
      } else {
        if (maxTimesCount !== '' && (curTimesInRowFor[maxTimesCount] || 0) > (maxTimesInRowFor[maxTimesCount] || 0)) {
          maxTimesInRowFor[maxTimesCount] = curTimesInRowFor[maxTimesCount];
        }
        maxTimesCount = 'naked single';
        curTimesInRowFor['naked single'] = 1;
      }
      cells[firstCell].value = nc;
      if (reduceCandidatesForPeers(firstCell, nc)) {
        solutions = solve();
      }

      numRestoreCandidatePeers++;
      const startTimep2 = performance.now();
      peerCandidates.forEach(p => {
        cells[p.index].candidates = p.peers;
      });
      const endTimep2 = performance.now();
      timeRestoreCandidatePeers += endTimep1 - startTimep1;
      timeRestoreCandidatePeers += endTimep2 - startTimep2;
      cells[firstCell].value = 0;

      return solutions;
    };

    const stage_Other = (firstCell: number): string[] => {
      let solutions: string[] = [];
      const nextCandidates = [ ...cells[firstCell].candidates ];
      if (maxTimesCount === 'other') {
        maxTimesInRowFor['other'] += 1;
      } else {
        if (maxTimesCount !== '' && (curTimesInRowFor[maxTimesCount] || 0) > (maxTimesInRowFor[maxTimesCount] || 0)) {
          maxTimesInRowFor[maxTimesCount] = curTimesInRowFor[maxTimesCount];
        }
        maxTimesCount = 'other';
        maxTimesInRowFor['other'] = 1;
      }
      nextCandidates.forEach(nc => {
        const startTimep1 = performance.now();
        const peerCandidates: PeerCandidate[] = cells[firstCell].peers.map(p => ({ index: p, peers: cells[p].candidates }));
        const endTimep1 = performance.now();
        cells[firstCell].value = nc;
        if (reduceCandidatesForPeers(firstCell, nc)) {
          solutions.push(...solve());
        }

        numRestoreCandidatePeers++;
        const startTimep2 = performance.now();
        peerCandidates.forEach(p => {
          cells[p.index].candidates = p.peers;
        });
        const endTimep2 = performance.now();
        timeRestoreCandidatePeers += endTimep1 - startTimep1;
        timeRestoreCandidatePeers += endTimep2 - startTimep2;
        cells[firstCell].value = 0;
      });
      return solutions;
    };

    const solve = (): string[] => {
      if (s_isFilled()) {
        if (s_isSolved()) {
          return [cells.map(c => c.value).join('')];
        }
        return [];
      }
      stepCount++;

      let solutions: string[] = [];

      if (candidates.length > 0) {
        if (cells[candidates[0]].candidates.length > 1) {
          // only resort if there *could* be something with fewer candidates
          getCandidateIndexes();
        }
        const numCands = cells[candidates[0]].candidates.length;
        if (numCands === 1) {
          const firstCell = candidates.shift() !;
          solutions.push(...stage_NakedSingle(firstCell));
          candidates.unshift(firstCell);
        } else {
          const firstCell = candidates.shift() !;
          solutions.push(...stage_Other(firstCell));
          candidates.unshift(firstCell);
        }
      }

      return solutions;
    };

    if (!Puzzle.isValid(cells.map(c => c.value).join(''))) return { ...EmptySolutionOutput, solution: 'invalid' };

    const st = performance.now();
    setAllCandidates();
    getCandidateIndexes();
    const sols = solve();
    const et = performance.now();

    console.log(`terminated after ${stepCount} steps over ${et-st}ms.\nRan reduceCandidatesForPeers ${numReduceCandidatesForPeers} times over ${timeReduceCandidatesForPeers}ms.\nRan setCandidatesForPeers ${numSetCandidatesForPeers} times over ${timeSetCandidatesForPeers}ms.\nRan getCandidateIndexes ${numGetCandidateIndexes} times over ${timeGetCandidateIndexes}ms.\nRan restoreCandidatePeers ${numRestoreCandidatePeers} times over ${timeRestoreCandidatePeers}ms.\nMax naked singles in a row: ${maxTimesInRowFor['naked single']}.`);
    if (sols.length === 1) {
      return { ...EmptySolutionOutput, solution: sols[0] };
    } else if (sols.length > 1) {
      return { ...EmptySolutionOutput, solution: 'multiple solutions', multipleSolutions: sols };
    } else {
      return { ...EmptySolutionOutput, solution: 'no solution' };
    }
  };
}
