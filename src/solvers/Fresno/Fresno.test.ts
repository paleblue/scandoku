// import { Puzzle, LongSample, SamplePuzzle, FilledValuePuzzle, SolutionConfig, SolutionConfig_NoLogging, SolutionOutput, EmptySolutionOutput } from '@types';
import { SolutionPacket, indexesOfBox } from '@types';
import { CellGroup, CandidatesInCells } from './Fresno';
import { Fresno  } from '@solvers';

describe('Fresno', () => {

  test('cells with dual naked triples', async () => {
    const solver = new Fresno();
    const p = '009532418325184970481769532938451607147628009562973801006845093094310080000290000'; // has two naked triples in box 9
    const unitIndexes: number[] = indexesOfBox(9);
    const packet: SolutionPacket = solver.getSolutionPacket(p);

    const cellGroups = solver.getCandidateGroupsInUnit(packet, unitIndexes, 3, true);
    expect(cellGroups).toEqual([
      { values: [1,2,7], cells: [60,69,78] },
      { values: [4,5,6], cells: [71, 79, 80] },
    ]);
  });

  test('cells with single naked triple', async () => {
    const solver = new Fresno();
    const p = '009532418325184970481769532938451607147628009562973801006845093094310080000290000'; // has two naked triples in box 9
    const unitIndexes: number[] = indexesOfBox(9);
    const packet: SolutionPacket = solver.getSolutionPacket(p);
    
    // tweak the candidates to make a hidden triple
    packet.candidateTable[2].cells.push(60);
    packet.candidateTable[2].rows[6].cells.push(60);
    packet.candidateTable[2].cols[6].cells.push(60);
    packet.candidateTable[2].boxes[8].cells.push(60);

    const cellGroups = solver.getCandidateGroupsInUnit(packet, unitIndexes, 3, true);
    expect(cellGroups).toEqual([
      { values: [4,5,6], cells: [71, 79, 80] },
    ]);
  });

  test('cells with hidden triple', async () => {
    const solver = new Fresno();
    const p = '009532418325184970481769532938451607147628000562973801006845003004310080000000000'; // has one hidden triple in box 9
    const unitIndexes: number[] = indexesOfBox(9);
    const packet: SolutionPacket = solver.getSolutionPacket(p);

    const cellGroups = solver.getCandidateGroupsInUnit(packet, unitIndexes, 3, false);
    expect(cellGroups).toEqual([
      { values: [4,5,6], cells: [71, 79, 80] },
    ]);
  });

  describe('candidate reduction methods', () => {
    test('hidden triple with excess', async () => {
      const solver = new Fresno();
      const p = '009532418325184970481769532938451607147628009562973801006845093094310080000090000';

      const solutionPacket = solver.getSolutionPacket(p);
      // expect '2' is an excess candidate in the hidden triple in box 9
      expect(solutionPacket.candidateTable[1].boxes[8].cells.includes(79)).toBeTruthy();
      solver.hiddenTriple.stage(solutionPacket, solver);
      // look to see if '2' was removed
      expect(solutionPacket.candidateTable[1].boxes[8].cells.includes(79)).toBeFalsy();
    });

    test('pointing pairs', async () => {
      const solver = new Fresno();
      const p = '017903600000080000900000507072010430000402070064370250701000065000030000005601720';

      const solutionPacket = solver.getSolutionPacket(p);
      // expect '3's in middle row of box 1
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(9)).toBeTruthy();
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(10)).toBeTruthy();
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(11)).toBeTruthy();
      solver.pointingPairs.stage(solutionPacket, solver);
      // look to see if the '3's were removed
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(9)).toBeFalsy();
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(10)).toBeFalsy();
      expect(solutionPacket.candidateTable[2].boxes[0].cells.includes(11)).toBeFalsy();
    });

    test('box line reduction', async () => {
      const solver = new Fresno();
      const p = '016007803090800000870001060048000300650009082039000650060900020080002936924600510';

      const solutionPacket = solver.getSolutionPacket(p);
      // expect '4's in box 3
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(15)).toBeTruthy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(17)).toBeTruthy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(24)).toBeTruthy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(26)).toBeTruthy();
      solver.boxLineReduction.stage(solutionPacket, solver);
      // look to see if the '4's were removed
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(15)).toBeFalsy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(17)).toBeFalsy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(24)).toBeFalsy();
      expect(solutionPacket.candidateTable[3].boxes[2].cells.includes(26)).toBeFalsy();
    });
  });

  describe('finding clusters', () => {
    test('naked triple with three pairs', async () => {
      const solver = new Fresno();
      const input: CandidatesInCells[] = [
        { cellIndex: 60, candidates: [ 1, 2 ] },
        { cellIndex: 69, candidates: [ 2, 7 ] },
        { cellIndex: 71, candidates: [ 5, 6, 9 ] },
        { cellIndex: 78, candidates: [ 1, 7 ] },
        { cellIndex: 79, candidates: [ 4, 5, 9 ] },
      ];
      const expectedOutput: CellGroup[] = [
        { cells: [ 60, 69, 78 ], candidates: [ 1, 2, 7 ] },
      ];
      expect(solver.findNakedCluster(input, 3)).toEqual(expectedOutput);
      // console.log(`${solver.numFindNakedCluster} findNakedCluster in ${solver.stepsFindNakedCluster} steps over ${solver.timeFindNakedCluster}ms`);
    });
    test('naked triple with one value being used elsewhere (not a hidden triple)', async () => {
      const solver = new Fresno();
      const input: CandidatesInCells[] = [
        { cellIndex: 60, candidates: [ 1, 2, 7 ] },
        { cellIndex: 61, candidates: [ 2, 9 ] },
        { cellIndex: 69, candidates: [ 2, 7 ] },
        { cellIndex: 71, candidates: [ 5, 6, 9 ] },
        { cellIndex: 78, candidates: [ 1, 2, 7 ] },
      ];
      const expectedOutput: CellGroup[] = [
        { cells: [ 60, 69, 78 ], candidates: [ 1, 2, 7 ] },
      ];
      expect(solver.findNakedCluster(input, 3)).toEqual(expectedOutput);
    });
    test('naked quad with four quads', async () => {
      const solver = new Fresno();
      const input: CandidatesInCells[] = [
        { cellIndex: 60, candidates: [ 1, 2, 7, 9 ] },
        { cellIndex: 69, candidates: [ 1, 2, 7, 9 ] },
        { cellIndex: 70, candidates: [ 1, 2, 7, 9 ] },
        { cellIndex: 71, candidates: [ 5, 6, 9 ] },
        { cellIndex: 78, candidates: [ 1, 2, 7, 9 ] },
        { cellIndex: 79, candidates: [ 4, 5, 9 ] },
      ];
      const expectedOutput: CellGroup[] = [
        { cells: [ 60, 69, 70, 78 ], candidates: [ 1, 2, 7, 9 ] },
      ];
      expect(solver.findNakedCluster(input, 4)).toEqual(expectedOutput);
    });
    test('naked quad with four pairs', async () => {
      const solver = new Fresno();
      const input: CandidatesInCells[] = [
        { cellIndex: 60, candidates: [ 1, 2 ] },
        { cellIndex: 69, candidates: [ 2, 7 ] },
        { cellIndex: 70, candidates: [ 7, 9 ] },
        { cellIndex: 71, candidates: [ 5, 6, 9 ] },
        { cellIndex: 78, candidates: [ 1, 9 ] },
        { cellIndex: 79, candidates: [ 4, 5, 9 ] },
      ];
      const expectedOutput: CellGroup[] = [
        { cells: [ 60, 69, 70, 78 ], candidates: [ 1, 2, 7, 9 ] },
      ];
      expect(solver.findNakedCluster(input, 4)).toEqual(expectedOutput);
      // console.log(`${solver.numFindNakedCluster} findNakedCluster in ${solver.stepsFindNakedCluster} steps over ${solver.timeFindNakedCluster}ms`);
    });
    test('three sets of pairs', async () => {
      const solver = new Fresno();
      const input: CandidatesInCells[] = [
        { cellIndex: 60, candidates: [ 1, 2 ] },
        { cellIndex: 69, candidates: [ 1, 2 ] },
        { cellIndex: 70, candidates: [ 7, 9 ] },
        { cellIndex: 71, candidates: [ 5, 8 ] },
        { cellIndex: 78, candidates: [ 7, 9 ] },
        { cellIndex: 79, candidates: [ 5, 8 ] },
      ];
      const expectedOutput: CellGroup[] = [
        { cells: [ 60, 69 ], candidates: [ 1, 2 ] },
        { cells: [ 70, 78 ], candidates: [ 7, 9 ] },
        { cells: [ 71, 79 ], candidates: [ 5, 8 ] },
      ];
      expect(solver.findNakedCluster(input, 2)).toEqual(expectedOutput);
      // console.log(`${solver.numFindNakedCluster} findNakedCluster in ${solver.stepsFindNakedCluster} steps over ${solver.timeFindNakedCluster}ms`);
    });
  });

});
