import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket } from '@types';
import { EIGHT } from '@constants';

export class HiddenSingle implements iSolverStage {

  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numHiddenSingle++;
    const startTime = performance.now();
    let didSomething = false;
    EIGHT.forEach(n => {
      if (packet.candidateTable[n].count > 0) {
        // this candidate exists somewhere
        packet.candidateTable[n].rows.forEach(r => {
          if (r.cells.length === 1) {
            parent.DO_LOGGING && parent.s_log(`hiddenSingle of ${n+1} at r${packet.cells[r.cells[0]].row}c${packet.cells[r.cells[0]].col}`);
            didSomething = true;
            parent.setAndReduceCandidatesForPeers(packet, n+1, r.cells[0]);
          }
        });
        packet.candidateTable[n].cols.forEach(r => {
          if (r.cells.length === 1) {
            parent.DO_LOGGING && parent.s_log(`hiddenSingle of ${n+1} at r${packet.cells[r.cells[0]].row}c${packet.cells[r.cells[0]].col}`);
            didSomething = true;
            parent.setAndReduceCandidatesForPeers(packet, n+1, r.cells[0]);
          }
        });
        packet.candidateTable[n].boxes.forEach(r => {
          if (r.cells.length === 1) {
            parent.DO_LOGGING && parent.s_log(`hiddenSingle of ${n+1} at r${packet.cells[r.cells[0]].row}c${packet.cells[r.cells[0]].col}`);
            didSomething = true;
            parent.setAndReduceCandidatesForPeers(packet, n+1, r.cells[0]);
          }
        });
      }
    });
    parent.solutionStatistics.timeHiddenSingle += (performance.now() - startTime);
    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
