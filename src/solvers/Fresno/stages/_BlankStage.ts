import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket } from '@types';

export class BlankStage implements iSolverStage {
  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numPointingPairs++;
    const startTime = performance.now();
    let didSomething = false;

    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    parent.solutionStatistics.timePointingPairs += (performance.now() - startTime);
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
