import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket, cellLocationFromIndex, indexesOfRow, indexesOfColumn } from '@types';

export class PointingPairs implements iSolverStage {
  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numPointingPairs++;
    const startTime = performance.now();
    let didSomething = false;

    packet.candidateTable.forEach((n, ni) => {
      n.boxes.forEach((b, bi) => {
        if (b.cells.length > 1) {
          const cellRows = b.cells.reduce<number[]>((prev, v) => [ ...prev, ...(prev.includes(cellLocationFromIndex(v).row) ? [] : [cellLocationFromIndex(v).row]) ], []);
          if (cellRows.length === 1) {
            const row = cellRows[0];

            if (n.rows[row-1].cells.length > b.cells.length) {
              // there are other candidates in this row
              const otherCellsInRow = indexesOfRow(row).filter(r => !b.cells.includes(r));
              n.rows[row-1].cells.forEach(c => {
                if (otherCellsInRow.includes(c)) {
                  didSomething = true;
                  parent.DO_LOGGING && parent.s_log(`pointing pairs in box ${bi+1} on row ${row} eliminates a ${ni+1} in column ${cellLocationFromIndex(c).col}`);
                  parent.removeCandidate(packet, ni+1, c);
                }
              });
            }
          }

          const cellCols = b.cells.reduce<number[]>((prev, v) => [ ...prev, ...(prev.includes(cellLocationFromIndex(v).col) ? [] : [cellLocationFromIndex(v).col]) ], []);
          if (cellCols.length === 1 && b.cells.length > 1) {
            const col = cellCols[0];

            if (n.cols[col-1].cells.length > b.cells.length) {
              // there are other candidates in this row
              const otherCellsInCol = indexesOfColumn(col).filter(r => !b.cells.includes(r));
              n.cols[col-1].cells.forEach(c => {
                if (otherCellsInCol.includes(c)) {
                  didSomething = true;
                  parent.DO_LOGGING && parent.s_log(`pointing pairs in box ${bi+1} on column ${col} eliminates a ${ni+1} in row ${cellLocationFromIndex(c).row}`);
                  parent.removeCandidate(packet, ni+1, c);
                }
              });
            }
          }
        }
      });
    });

    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    parent.solutionStatistics.timePointingPairs += (performance.now() - startTime);
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
