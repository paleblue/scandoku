import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket } from '@types';
import { NINE } from '@constants';

export class NakedSingle implements iSolverStage {

  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numNakedSingle++;
    const startTime = performance.now();
    let didSomething = false;
    packet.cellCandidateCounts.forEach((d, di) => {
      if (d === 1 && packet.cells[di].value === 0) {
        didSomething = true;
        const singleValues = NINE.filter(n => packet.candidateTable[n-1].rows[packet.cells[di].row-1].cells.includes(di));
        singleValues.forEach(n => {
          parent.DO_LOGGING && parent.s_log(`nakedSingle of ${n} at r${packet.cells[di].row}c${packet.cells[di].col}`);
          parent.setAndReduceCandidatesForPeers(packet, n, di);
        });
      }
    });
    parent.solutionStatistics.timeNakedSingle += (performance.now() - startTime);
    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
