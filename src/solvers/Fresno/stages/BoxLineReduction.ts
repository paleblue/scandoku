import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket, cellBoxByIndex, indexesOfBox, cellLocationFromIndex } from '@types';

export class BoxLineReduction implements iSolverStage {
  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numBoxLineReduction++;
    const startTime = performance.now();
    let didSomething = false;

    packet.candidateTable.forEach((n, ni) => {
      n.rows.forEach((r, ri) => {
        if (r.cells.length > 1) {
          // this number exists as a candidate in this row
          // is it contained in a single box?
          const cellBoxes = r.cells.reduce<number[]>((prev, v) => [ ...prev, ...(prev.includes(cellBoxByIndex(v)) ? [] : [cellBoxByIndex(v)]) ], []);
          if (cellBoxes.length === 1) {
            const box = cellBoxes[0];

            if (n.boxes[box-1].cells.length > r.cells.length) {
              // there are other candidates in this box
              const otherCellsInBox = indexesOfBox(box).filter(b => !r.cells.includes(b));
              n.boxes[box-1].cells.forEach(c => {
                if (otherCellsInBox.includes(c)) {
                  didSomething = true;
                  parent.DO_LOGGING && parent.s_log(`box line reduction in box ${box} on row ${ri+1} eliminates a ${ni+1} in r${cellLocationFromIndex(c).row}c${cellLocationFromIndex(c).col}`);
                  parent.removeCandidate(packet, ni+1, c);
                }
              });
            }
          }
        }
      });

      n.cols.forEach((r, ri) => {
        if (r.cells.length > 1) {
          // this number exists as a candidate in this column
          // is it contained in a single box?
          const cellBoxes = r.cells.reduce<number[]>((prev, v) => [ ...prev, ...(prev.includes(cellBoxByIndex(v)) ? [] : [cellBoxByIndex(v)]) ], []);
          if (cellBoxes.length === 1) {
            const box = cellBoxes[0];

            if (n.boxes[box-1].cells.length > r.cells.length) {
              // there are other candidates in this box
              const otherCellsInBox = indexesOfBox(box).filter(b => !r.cells.includes(b));
              n.boxes[box-1].cells.forEach(c => {
                if (otherCellsInBox.includes(c)) {
                  didSomething = true;
                  parent.DO_LOGGING && parent.s_log(`box line reduction in box ${box} on column ${ri+1} eliminates a ${ni+1} in r${cellLocationFromIndex(c).row}c${cellLocationFromIndex(c).col}`);
                  parent.removeCandidate(packet, ni+1, c);
                }
              });
            }
          }
        }
      });
    });

    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    parent.solutionStatistics.timeBoxLineReduction += (performance.now() - startTime);
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
