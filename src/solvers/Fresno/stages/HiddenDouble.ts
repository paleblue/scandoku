import { iSolverStage, Fresno } from '@solvers';
import { SolutionPacket, indexesOfRow, indexesOfColumn, indexesOfBox, cellLocationFromIndex } from '@types';
import { NINE } from '@constants';

export class HiddenDouble implements iSolverStage {

  private removePeerCandidates = (packet: SolutionPacket, parent: Fresno, indexes: number[], values: number[]): boolean => {
    let didSomething: boolean = false;
    indexes.forEach(i => {
      if (packet.cells[i].value === 0) {
        parent.getCandidatesOfCell(packet, i).forEach(c => {
          if (values.includes(c)) {
            didSomething = true;
            parent.removeCandidate(packet, c, i);
            parent.DO_LOGGING && parent.s_log(`removing peer candidate ${c} from HiddenDoubles [${values.join(',')}] in r${cellLocationFromIndex(i).row}c${cellLocationFromIndex(i).col}`);
          }
        });
      }
    });
    return didSomething;
  };

  public stage = (packet: SolutionPacket, parent: Fresno): boolean => {
    parent.solutionStatistics.numHiddenDouble++;
    const startTime = performance.now();
    let didSomething = false;

    NINE.forEach(n => {
      const rowPossibles = parent.getCandidateGroupsInUnit(packet, indexesOfRow(n), 2, false);

      if (rowPossibles.length > 0) {
        rowPossibles.forEach(r => {
          didSomething = this.removePeerCandidates(packet, parent, indexesOfRow(n).filter(i => !r.cells.includes(i)), r.values) || didSomething; 
        });
      }

      const colPossibles = parent.getCandidateGroupsInUnit(packet, indexesOfColumn(n), 2, false);

      if (colPossibles.length > 0) {
        colPossibles.forEach(r => {
          didSomething = this.removePeerCandidates(packet, parent, indexesOfColumn(n).filter(i => !r.cells.includes(i)), r.values) || didSomething; 
        });
      }

      const boxPossibles = parent.getCandidateGroupsInUnit(packet, indexesOfBox(n), 2, false);

      if (boxPossibles.length > 0) {
        boxPossibles.forEach(r => {
          didSomething = this.removePeerCandidates(packet, parent, indexesOfBox(n).filter(i => !r.cells.includes(i)), r.values) || didSomething; 
        });
      }
    });

    if (didSomething && !parent.hasEmptyCandidates(packet)) {
      parent.stepCount++;
    }
    parent.solutionStatistics.timeHiddenDouble += (performance.now() - startTime);
    return didSomething && !parent.hasEmptyCandidates(packet);
  };
}
