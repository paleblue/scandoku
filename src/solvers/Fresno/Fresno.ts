import { iSolver, iSolverStage } from '@solvers';
import { Puzzle, PuzzleCell, SolutionConfig, SolutionOutput, SolutionConfig_AllLogging,
  EmptySolutionOutput, SolutionConfig_NoLogging, cellLocationFromIndex, 
  SolutionStatistics, EmptySolutionStatistics, SolutionResult, 
  CandidateSet, SolutionPacket, EmptySolutionPacket } from '@types';
import { BaseCells, NINE, EIGHT, MAX_SOLVE_STEPS } from '@constants';
import { NakedSingle } from './stages/NakedSingle';
import { HiddenSingle } from './stages/HiddenSingle';
import { NakedDouble } from './stages/NakedDouble';
import { HiddenDouble } from './stages/HiddenDouble';
import { NakedTriple } from './stages/NakedTriple';
import { HiddenTriple } from './stages/HiddenTriple';
import { PointingPairs } from './stages/PointingPairs';
import { BoxLineReduction } from './stages/BoxLineReduction';

export type UnitPossible = { unit: number, possibles: { [key: number]: number[] } };
export type CellClusters = { values: number[], cells: number[] };
export type CandidatesInCells = { cellIndex: number, candidates: number[] };
export type CellInUnits = { value: number, cells: number[], peerValues: number[], peerCells: number[] };
export type CellGroup = { cells: number[], candidates: number[] };

export class Fresno implements iSolver {
  public stepCount: number = 1;
  public lineCount: number = 1;
  public solutionStatistics: SolutionStatistics;
  public solutionConfig: SolutionConfig;

  public numGuess: number = 0;
  public timeGuess: number = 0;
  public numCopyCells: number = 0;
  public timeCopyCells: number = 0;
  public numSetAllCandidates: number = 0;
  public timeSetAllCandidates: number = 0;
  public numGetCandidateIndexes: number = 0;
  public timeGetCandidateIndexes: number = 0;
  public numReduceCandidatesForPeers: number = 0;
  public timeReduceCandidatesForPeers: number = 0;
  public numHasEmptyCandidates: number = 0;
  public timeHasEmptyCandidates: number = 0;
  public numGetCandidatesOfCell: number = 0;
  public timeGetCandidatesOfCell: number = 0;
  public numGetCandidateGroupsInUnit: number = 0;
  public timeGetCandidateGroupsInUnit: number = 0;
  public numFindNakedCluster: number = 0;
  public stepsFindNakedCluster: number = 0;
  public timeFindNakedCluster: number = 0;

  public get DO_LOGGING(): boolean { return this.solutionConfig.allLogging; };
  public get DO_FINAL_LOGGING(): boolean { return this.solutionConfig.finalLogging; };
  public get DO_TABLE_LOGGING(): boolean { return this.solutionConfig.tableLogging; };

  public nakedSingle: iSolverStage;
  public hiddenSingle: iSolverStage;
  public nakedDouble: iSolverStage;
  public hiddenDouble: iSolverStage;
  public nakedTriple: iSolverStage;
  public hiddenTriple: iSolverStage;
  public pointingPairs: iSolverStage;
  public boxLineReduction: iSolverStage;

  public constructor() {
    this.nakedSingle = new NakedSingle();
    this.hiddenSingle = new HiddenSingle();
    this.nakedDouble = new NakedDouble();
    this.hiddenDouble = new HiddenDouble();
    this.nakedTriple = new NakedTriple();
    this.hiddenTriple = new HiddenTriple();
    this.pointingPairs = new PointingPairs();
    this.boxLineReduction = new BoxLineReduction();
    this.solutionStatistics = { ...EmptySolutionStatistics };
    this.solutionConfig = { ...SolutionConfig_NoLogging };
  }

  private resetStats = () => {
    this.numGuess = 0;
    this.timeGuess = 0;
    this.numCopyCells = 0;
    this.timeCopyCells = 0;
    this.numSetAllCandidates = 0;
    this.timeSetAllCandidates = 0;
    this.numGetCandidateIndexes = 0;
    this.timeGetCandidateIndexes = 0;
    this.numReduceCandidatesForPeers = 0;
    this.timeReduceCandidatesForPeers = 0;
    this.numHasEmptyCandidates = 0;
    this.timeHasEmptyCandidates = 0;
    this.numGetCandidatesOfCell = 0;
    this.timeGetCandidatesOfCell = 0;
    this.numGetCandidateGroupsInUnit = 0;
    this.timeGetCandidateGroupsInUnit = 0;
    this.numFindNakedCluster = 0;
    this.stepsFindNakedCluster = 0;
    this.timeFindNakedCluster = 0;

    this.stepCount = 1;
    this.lineCount = 1;
  };

  public getSolutionPacket = (puzzle: string): SolutionPacket => {
    if (puzzle.length !== 81 || !Puzzle.isValid(puzzle)) return { ...EmptySolutionPacket };

    const solutionCells = BaseCells.map((c, i) => {
      const v = parseInt(puzzle.substr(i, 1));
      return {
        ...c,
        value: v,
        fixed: v > 0,
      };
    });

    const solutionCandidateTable: CandidateSet[] = Array(9);
    const solutionCellCandidateCounts: number[] = Array(81).fill(0);

    const solutionPacket = {
      cells: solutionCells,
      candidateTable: solutionCandidateTable,
      cellCandidateCounts: solutionCellCandidateCounts,
    };
    this.setAllCandidates(solutionPacket);

    return solutionPacket;
  };

	public getSolution = async (puzzle: string, solutionConfig: SolutionConfig = { ...SolutionConfig_NoLogging } ): Promise<SolutionOutput> => {
    if (puzzle.length !== 81 || !Puzzle.isValid(puzzle)) return { ...EmptySolutionOutput, solution: 'invalid' };

    this.resetStats();
    this.solutionStatistics = { ...EmptySolutionStatistics };
    this.solutionConfig = solutionConfig;

    const startTime = performance.now();

    const solutionPacket = this.getSolutionPacket(puzzle);

    const stage_Guess = (packet: SolutionPacket) => {
      this.numGuess++;
      this.setAllCandidates(packet);
      const candidates = this.getCandidateIndexes(packet);
      // s_log(`candidates for guess: ${candidates.map(c => `r${cells[c].row}c${cells[c].col}`).join(', ')}`);
      if (candidates.length > 0) {
        let startTime = performance.now();
        const cIndex = candidates[0];
        const cands = NINE.filter(n => !packet.cells[cIndex].peers.some(r => packet.cells[r].value === n));

        let solutions: SolutionResult[] = [];
        this.timeGuess += (performance.now() - startTime);
        cands.forEach(cand => {
          this.stepCount++;
          const innerPacket = this.s_copyPacket(packet);
          this.DO_LOGGING && this.s_log(`guess of ${cand} in r${innerPacket.cells[cIndex].row}c${innerPacket.cells[cIndex].col}`);
          this.setAndReduceCandidatesForPeers(innerPacket, cand, cIndex);
          if (!this.hasEmptyCandidates(innerPacket)) {
            solutions = [ ...solutions, ...solve(innerPacket) ];
          }
        });
        return solutions;
      }
      return [];
    };

    const solve = (packet: SolutionPacket, solutions: SolutionResult[] = []): SolutionResult[] => {
      if (this.s_isFilled(packet.cells)) {
        if (this.s_isSolved(packet.cells)) {
          return [ ...solutions, packet.cells.map(c => c.value).join('')];
        }
        return [];
      }

      let madeProgress = true;
      let stepsLeft = 100; // prevent craziness

      // logic based attempts,
      // continue while it still makes progress
      while(madeProgress && stepsLeft > 0 && this.stepCount < MAX_SOLVE_STEPS) {
        // loop through all logic solutions 'OR'd together
        madeProgress = 
          (solutionConfig.doNakedSingles && this.nakedSingle.stage(packet, this)) ||
          (solutionConfig.doHiddenSingles && this.hiddenSingle.stage(packet, this)) ||
          (solutionConfig.doNakedDoubles && this.nakedDouble.stage(packet, this)) ||
          (solutionConfig.doHiddenDoubles && this.hiddenDouble.stage(packet, this)) ||
          (solutionConfig.doNakedTriples && this.nakedTriple.stage(packet, this)) ||
          (solutionConfig.doHiddenTriples && this.hiddenTriple.stage(packet, this)) ||
          (solutionConfig.doPointingPairs && this.pointingPairs.stage(packet, this)) ||
          (solutionConfig.doBoxLineReduction && this.boxLineReduction.stage(packet, this));
        stepsLeft--;
      }
      if (this.stepCount >= MAX_SOLVE_STEPS) {
        // something is wrong
        return [];
      } else if (this.s_isFilled(packet.cells)) {
        this.DO_LOGGING && this.s_log(`puzzle filled`);
        if (this.s_isSolved(packet.cells)) {
          this.DO_LOGGING && this.s_log(`puzzle SOLVED`);
          return [ ...solutions, packet.cells.map(c => c.value).join('')];
        }

        return [];
      } else {
        this.DO_LOGGING && this.s_log(`stages failed to do anything`);
        this.DO_LOGGING && this.s_log(`http://192.168.0.37:3000/puzzle/${packet.cells.map(c => c.value || 0).join('')}`);
        // DO_LOGGING && s_log(`\n${Puzzle.outputTextPuzzle(cells.map(c => c.value || 0).join(''))}`);
        // DO_LOGGING && s_log(indexesOfRow(1).map(r => cells[r].value > 0 ? '#' : cells[r].candidates.join('')).join(','));

        if (this.hasEmptyCandidates(packet)) {
          this.DO_LOGGING && this.s_log('exiting for empty candidates', `${packet.cells.filter(c => c.value > 0).length} complete cells`);
          return [];
        }

        return stage_Guess(packet);
      }
    };

    const solutions = solve(solutionPacket);

    const endTime = performance.now();
    this.DO_FINAL_LOGGING && console.log(`sol6 (Fresno) took ${this.stepCount} steps in ${endTime - startTime}ms: [${solutions.join(',')}]`);
    this.DO_TABLE_LOGGING && console.table({
      ...this.solutionStatistics,
      numGuess: this.numGuess,
      timeGuess: this.timeGuess,
      numCopyCells: this.numCopyCells,
      timeCopyCells: this.timeCopyCells,
      numSetAllCandidates: this.numSetAllCandidates,
      timeSetAllCandidates: this.timeSetAllCandidates,
      numGetCandidateIndexes: this.numGetCandidateIndexes,
      timeGetCandidateIndexes: this.timeGetCandidateIndexes,
      numReduceCandidatesForPeers: this.numReduceCandidatesForPeers,
      timeReduceCandidatesForPeers: this.timeReduceCandidatesForPeers,
      numHasEmptyCandidates: this.numHasEmptyCandidates,
      timeHasEmptyCandidates: this.timeHasEmptyCandidates,
      numGetCandidatesOfCell: this.numGetCandidatesOfCell,
      timeGetCandidatesOfCell: this.timeGetCandidatesOfCell,
      numGetCandidateGroupsInUnit: this.numGetCandidateGroupsInUnit,
      timeGetCandidateGroupsInUnit: this.timeGetCandidateGroupsInUnit,
    });

    switch (solutions.length) {
      case 0:
        return {
          solution: 'no solution',
          stepCount: this.stepCount,
          totalTime: (endTime - startTime),
          statistics: this.solutionStatistics,
        } as SolutionOutput;
      case 1:
        return {
          solution: solutions[0],
          stepCount: this.stepCount,
          totalTime: (endTime - startTime),
          statistics: this.solutionStatistics,
        } as SolutionOutput;
      default:
        return {
          solution: 'multiple solutions',
          stepCount: this.stepCount,
          totalTime: (endTime - startTime),
          multipleSolutions: solutions,
          statistics: this.solutionStatistics,
        } as SolutionOutput;
    }
  };

  public setAllCandidates = (packet: SolutionPacket) => {
    this.numSetAllCandidates++;
    const startTime = performance.now();
    packet.cellCandidateCounts.forEach((_cc, i) => packet.cellCandidateCounts[i] = 0);
    NINE.forEach(n => {
      packet.candidateTable[n-1] = {
        count: 0,
        rows: NINE.map(_n => ({ count: 0, cells: [] })),
        cols: NINE.map(_n => ({ count: 0, cells: [] })),
        boxes: NINE.map(_n => ({ count: 0, cells: [] })),
        cells: [],
      };
      packet.cells.forEach(c => {
        if (c.value === 0) {
          if (!c.peers.some(r => packet.cells[r].value === n)) {
            // n is a valid candidate for cell c
            packet.cellCandidateCounts[c.index]++;
            packet.candidateTable[n-1].count++;
            packet.candidateTable[n-1].cells.push(c.index);
            packet.candidateTable[n-1].rows[c.row-1].count++;
            packet.candidateTable[n-1].rows[c.row-1].cells.push(c.index);
            packet.candidateTable[n-1].cols[c.col-1].count++;
            packet.candidateTable[n-1].cols[c.col-1].cells.push(c.index);
            packet.candidateTable[n-1].boxes[c.box-1].count++;
            packet.candidateTable[n-1].boxes[c.box-1].cells.push(c.index);
          }
        }
      });
    });
    this.timeSetAllCandidates += (performance.now() - startTime);
  };

  public s_isFilled = (cells: PuzzleCell[]): boolean => {
    return cells.every(c => c.value > 0);
  };

  public s_isSolved = (cells: PuzzleCell[]): boolean => {
    return Puzzle.isSolved(cells.map(c => c.value).join(''));
  };

  public s_log = (firstArg: unknown, ...lastArgs: unknown[]) => {
    console.log(`${(this.lineCount++).toString().padStart(5, '0')}   ${(this.stepCount).toString().padStart(3, '0')}   ${firstArg}`, ...lastArgs);
  };

  public s_copyCells = (cells: PuzzleCell[]): PuzzleCell[] => {
    this.numCopyCells++;
    const startTime = performance.now();
    const newCells = cells.map(c => ({
      ...c,
      peers: [...c.peers],
    }));
    this.timeCopyCells += (performance.now() - startTime);
    return newCells;
  };

  public s_copyCandidateTable = (candidateTable: CandidateSet[]): CandidateSet[] => {
    return candidateTable.map(s => ({ 
      count: s.count, 
      cells: [ ...s.cells ], 
      rows: s.rows.map(r => ({ count: r.count, cells: [ ...r.cells ] })),
      cols: s.cols.map(r => ({ count: r.count, cells: [ ...r.cells ] })),
      boxes: s.boxes.map(r => ({ count: r.count, cells: [ ...r.cells ] })),
    }));
  };

  public s_copyPacket = (packet: SolutionPacket) => {
    const newPacket: SolutionPacket = {
      cells: this.s_copyCells(packet.cells),
      candidateTable: this.s_copyCandidateTable(packet.candidateTable),
      cellCandidateCounts: [ ...packet.cellCandidateCounts ],
    };
    return newPacket;
  };

  public getCandidateIndexes = (packet: SolutionPacket) => {
    this.numGetCandidateIndexes++;
    const startTime = performance.now();
    const newCandidates = packet.cellCandidateCounts.map((c, ci) => ({ value: c, index: ci })).filter(c => packet.cells[c.index].value === 0).sort((a, b) => a.value - b.value).map(c => c.index);
    this.timeGetCandidateIndexes += (performance.now() - startTime);
    return newCandidates;
  };

  public getCandidatesOfCell = (packet: SolutionPacket, index: number) => {
    if (packet.cells[index].value > 0) return [];

    const cands: number[] = [];
    const loc = cellLocationFromIndex(index);
    EIGHT.forEach(n => {
      if (packet.candidateTable[n].rows[loc.row-1].cells.includes(index) && packet.candidateTable[n].cols[loc.col-1].cells.includes(index)) {
        cands.push(n+1);
      }
    });
    return cands;
  };

  public removeCandidate = (packet: SolutionPacket, value: number, index: number) => {
    if (packet.candidateTable[value-1].cells.includes(index) && packet.cellCandidateCounts[index] > 0) {
      if (packet.candidateTable[value-1].rows[packet.cells[index].row-1].cells.includes(index)) {
        packet.candidateTable[value-1].rows[packet.cells[index].row-1].cells = packet.candidateTable[value-1].rows[packet.cells[index].row-1].cells.filter(f => f !== index);
        packet.candidateTable[value-1].rows[packet.cells[index].row-1].count = packet.candidateTable[value-1].rows[packet.cells[index].row-1].cells.length;
      }
      if (packet.candidateTable[value-1].cols[packet.cells[index].col-1].cells.includes(index)) {
        packet.candidateTable[value-1].cols[packet.cells[index].col-1].cells = packet.candidateTable[value-1].cols[packet.cells[index].col-1].cells.filter(f => f !== index);
        packet.candidateTable[value-1].cols[packet.cells[index].col-1].count = packet.candidateTable[value-1].cols[packet.cells[index].col-1].cells.length;
      }
      if (packet.candidateTable[value-1].boxes[packet.cells[index].box-1].cells.includes(index)) {
        packet.candidateTable[value-1].boxes[packet.cells[index].box-1].cells = packet.candidateTable[value-1].boxes[packet.cells[index].box-1].cells.filter(f => f !== index);
        packet.candidateTable[value-1].boxes[packet.cells[index].box-1].count = packet.candidateTable[value-1].boxes[packet.cells[index].box-1].cells.length;
      }

      packet.candidateTable[value-1].cells = packet.candidateTable[value-1].cells.filter(f => f !== index);
      packet.candidateTable[value-1].count = packet.candidateTable[value-1].cells.length;

      packet.cellCandidateCounts[index]--;
    }
  };

  public setAndReduceCandidatesForPeers = (packet: SolutionPacket, value: number, index: number) => {
    this.numReduceCandidatesForPeers++;
    const startTime = performance.now();
    const cands = NINE.filter(n => !packet.cells[index].peers.some(r => packet.cells[r].value === n));
    cands.forEach(c => {
      this.removeCandidate(packet, c, index);
    });
    packet.cells[index].value = value;
    packet.cellCandidateCounts[index] = 0;
    packet.cells[index].peers.forEach(p => {
      if (packet.cells[p].value === 0) {
        this.removeCandidate(packet, value, p);
      }
    });
    this.timeReduceCandidatesForPeers += (performance.now() - startTime);
  };

  public hasEmptyCandidates = (packet: SolutionPacket) => {
    this.numHasEmptyCandidates++;
    const startTime = performance.now();
    const hasEmpty = packet.cellCandidateCounts.some((c, i) => c === 0 && packet.cells[i].value === 0);
    this.timeHasEmptyCandidates += (performance.now() - startTime);
    return hasEmpty;
  };

  public findNakedCluster = (candidates: CandidatesInCells[], clusterSize: number, matchAgainst: number[] = []): CellGroup[] => {
    let startTime: number = 0;
    this.stepsFindNakedCluster++;
    if (matchAgainst.length === 0) {
      this.numFindNakedCluster++;
      startTime = performance.now();
    }
    if (clusterSize > 1) {
      const results: CellGroup[] = [];
      candidates.forEach(c => {
        const otherCandidates = candidates.filter(f => f.cellIndex !== c.cellIndex);
        const candidateCombos = [ ...matchAgainst, ...c.candidates.filter(n => !matchAgainst.includes(n)) ];
        if (candidateCombos.length <= clusterSize) {
          if (candidateCombos.length === clusterSize) {
            results.push({
              cells: [c.cellIndex],
              candidates: candidateCombos.sort(),
            });
          }
          results.push(...this.findNakedCluster(otherCandidates, clusterSize, candidateCombos));
        }
      });

      if (matchAgainst.length === 0) {
        // original run, this is a final output
        const outResults: CellGroup[] = [];
        const candCells: { [key:string]: CellGroup } = {};
        results.forEach(r => {
          const cstr = r.candidates.join(',');
          if (candCells.hasOwnProperty(cstr)) {
            candCells[cstr].cells.push(...(r.cells.filter(f => !candCells[cstr].cells.includes(f))));
          } else {
            candCells[cstr] = { cells: [...r.cells], candidates: [...r.candidates] };
          }
        });
        this.timeFindNakedCluster += (performance.now() - startTime);
        for (let c in candCells) {
          if (candCells.hasOwnProperty(c) && candCells[c].cells.length === clusterSize) {
            candCells[c].cells.sort();
            candCells[c].candidates.sort();
            outResults.push(candCells[c]);
          }
        }
        outResults.sort((a, b) => a.cells[0] - b.cells[0]);
        return outResults;
      } else {
        return results;
      }
    } else {
      return [];
    }
  };

  public getCandidateGroupsInUnit = (packet: SolutionPacket, unitIndexes: number[], clusterSize: number, onlyNaked: boolean): CellClusters[] => {
    // TODO lots of room for improvement here
    this.numGetCandidateGroupsInUnit++;
    const startTime = performance.now();
    const matches: CellInUnits[] = [];
    const cands: CandidatesInCells[] = [];
    unitIndexes.forEach(i => {
      if (packet.cells[i].value === 0) {
        const candidates = this.getCandidatesOfCell(packet, i);
        cands.push({ cellIndex: i, candidates: candidates });
        candidates.forEach(n => {
          const cellMatchIndex = matches.findIndex(m => m.value === n);
          if (cellMatchIndex > -1) {
            matches[cellMatchIndex].cells.push(i);
          } else {
            matches.push({ value: n, cells: [i], peerValues: [], peerCells: [] });
          }
        });
      }
    });
    matches.sort((a, b) => a.value - b.value);
    cands.sort((a, b) => a.cellIndex - b.cellIndex);

    const outputMatches: CellClusters[] = [];

    if (onlyNaked) {
      const filteredCands: CandidatesInCells[] = cands.filter(c => c.candidates.length > 1 && c.candidates.length <= clusterSize);
      outputMatches.push(...this.findNakedCluster(filteredCands, clusterSize).map<CellClusters>(m => ({ values: m.candidates, cells: m.cells })));
    } else {
      // just run findNakedCluster backwards! cells -> candidates, candidates -> cells
      const filteredMatches: CellInUnits[] = matches.filter(c => c.cells.length > 1 && c.cells.length <= clusterSize);
      outputMatches.push(...this.findNakedCluster(filteredMatches.map<CandidatesInCells>(f => ({ cellIndex: f.value, candidates: [ ...f.cells ] })), clusterSize).map<CellClusters>(m => ({ values: m.cells, cells: m.candidates })));
    }

    this.timeGetCandidateGroupsInUnit += (performance.now() - startTime);
    return outputMatches;
  };
}
