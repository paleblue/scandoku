import { iSolver } from '@solvers';
import { Puzzle, cellLocationFromIndex, SolutionConfig, SolutionOutput, EmptySolutionOutput, SolutionConfig_NoLogging } from '@types';

export class Boston implements iSolver {
	public getSolution = async (puzzle: string, _solutionConfig: SolutionConfig = SolutionConfig_NoLogging ): Promise<SolutionOutput> => {
		const cellValues: number[] = Array.from(puzzle).map(s => parseInt(s));
		if (cellValues.includes(0) === false) return { ...EmptySolutionOutput, solution: cellValues.join('') };

		const unsolvedIndexes = cellValues.map((c, i) => c !== 0 ? -1 : i).filter(v => v > -1);
		let status: SolutionOutput = { ...EmptySolutionOutput, solution: 'no-solution' };

		const unsol = unsolvedIndexes[0];
		const loc = cellLocationFromIndex(unsol);
		const candidates = Puzzle.getCandidates(cellValues.join(''), loc.row, loc.col);
		for (let cani = 0; cani < candidates.length; cani++) {
			// we're trying variations of this cell
			// get solution for values, but with this cell incremented
			const tryval = candidates[cani];
			const strPuzzle = cellValues.join('');
			const newPuzzle = strPuzzle.substr(0, unsol) + tryval.toString() + strPuzzle.substr(unsol+1);
			if (unsolvedIndexes.length === 1) {
				// if we're on the last unsolved cell, we can test now
				if (Puzzle.isValid(newPuzzle)) {
					// we can skip the rest of the NINE foreach, we know there's only one option here
					status.solution = newPuzzle;
					break;
				}
			} else {
				// otherwise, recurse
				const nextSolution = await this.getSolution(newPuzzle);
				if (nextSolution.solution.length === 81) {
					if (status.solution.length === 81) {
						// there's already one solution! this must be multiple
						status = { ...EmptySolutionOutput, solution: 'multiple solutions' };
					} else {
						status = { ...EmptySolutionOutput, solution: nextSolution.solution };
					}
				}
			}
		}
		return { ...EmptySolutionOutput, solution: status.solution };
  };
}
