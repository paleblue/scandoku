import { iSolver } from '@solvers';
import { Puzzle, PuzzleCell, SolutionConfig, SolutionOutput, EmptySolutionOutput, SolutionConfig_NoLogging, indexesOfRow, indexesOfColumn, indexesOfBox, SolutionStatistics, EmptySolutionStatistics, SolutionResult } from '@types';
import { BaseCells, NINE } from '@constants';

export class Eastlake implements iSolver {
	public getSolution = async (puzzle: string, solutionConfig: SolutionConfig = SolutionConfig_NoLogging ): Promise<SolutionOutput> => {
    if (puzzle.length !== 81 || !Puzzle.isValid(puzzle)) return { ...EmptySolutionOutput, solution: 'invalid' };

    const solutionStatistics: SolutionStatistics = { ...EmptySolutionStatistics };

    let numGuess: number = 0;
    let timeGuess: number = 0;
    let numCopyCells: number = 0;
    let timeCopyCells: number = 0;
    let numSetAllCandidates: number = 0;
    let timeSetAllCandidates: number = 0;
    let numGetCandidateIndexes: number = 0;
    let timeGetCandidateIndexes: number = 0;
    let numReduceCandidatesForPeers: number = 0;
    let timeReduceCandidatesForPeers: number = 0;
    let numHasEmptyCandidates: number = 0;
    let timeHasEmptyCandidates: number = 0;

    const DO_LOGGING: boolean = solutionConfig.allLogging;
    const DO_FINAL_LOGGING: boolean = solutionConfig.finalLogging;
    const DO_TABLE_LOGGING: boolean = solutionConfig.tableLogging;

    let stepCount: number = 0;
    let lineCount: number = 0;
    const startTime = performance.now();
    const solutionCells = BaseCells.map((c, i) => {
      const v = parseInt(puzzle.substr(i, 1));
      return {
        ...c,
        value: v,
        fixed: v > 0,
      };
    });

    const setAllCandidates = (cells: PuzzleCell[]) => {
      numSetAllCandidates++;
      const startTime = performance.now();
      cells.forEach(c => {
        if (c.value === 0) {
          c.candidates = NINE.filter(n => !c.peers.some(r => cells[r].value === n));
        } else {
          c.candidates = [];
        }
      });
      timeSetAllCandidates += (performance.now() - startTime);
    };

    const s_isFilled = (cells: PuzzleCell[]): boolean => {
      return cells.every(c => c.value > 0);
    };

    const s_isSolved = (cells: PuzzleCell[]): boolean => {
      return Puzzle.isSolved(cells.map(c => c.value).join(''));
    };

    const s_log = (...args: unknown[]) => {
      const [firstArgs, ...lastArgs] = [...args];
      console.log(`${(lineCount++).toString().padStart(5, '0')}   ${firstArgs}`, ...lastArgs);
    };

    const s_copyCells = (cells: PuzzleCell[]): PuzzleCell[] => {
      numCopyCells++;
      const startTime = performance.now();
      const newCells = cells.map(c => ({
        ...c,
        candidates: [...c.candidates],
        peers: [...c.peers],
      }));
      timeCopyCells += (performance.now() - startTime);
      return newCells;
    };

    const getCandidateIndexes = (cells: PuzzleCell[]): number[] => {
      numGetCandidateIndexes++;
      const startTime = performance.now();
      const newCandidates = cells
        .filter(c => c.value === 0 && c.candidates.length > 0)
        .sort((a, b) => a.candidates.length - b.candidates.length)
        .map(c => c.index);
      timeGetCandidateIndexes += (performance.now() - startTime);
      return newCandidates;
    };

    const reduceCandidatesForPeers = (cells: PuzzleCell[], index: number) => {
      numReduceCandidatesForPeers++;
      const startTime = performance.now();
      const cand = cells[index].value;
      cells[index].peers.forEach(p => {
        if (cells[p].value === 0) {
          cells[p].candidates = cells[p].candidates.filter(f => f !== cand);
        }
      });
      timeReduceCandidatesForPeers += (performance.now() - startTime);
    };

    const hasEmptyCandidates = (cells: PuzzleCell[]): boolean => {
      numHasEmptyCandidates++;
      const startTime = performance.now();
      const hasEmpty = cells.some(c => c.value === 0 && c.candidates.length === 0);
      timeHasEmptyCandidates += (performance.now() - startTime);
      return hasEmpty;
    };

    const stage_NakedSingle = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numNakedSingle++;
      const startTime = performance.now();
      let didSomething = false;
      stepCount++;
      cells.forEach(c => {
        if (c.value === 0 && c.candidates.length === 1) {
          didSomething = true;
          c.value = c.candidates[0];
          DO_LOGGING && s_log(`nakedSingle of ${c.candidates[0]} in r${c.row}c${c.col}`);
          c.candidates = [];
          reduceCandidatesForPeers(cells, c.index);
        }
      });
      solutionStatistics.timeNakedSingle += (performance.now() - startTime);
      return didSomething && !hasEmptyCandidates(cells);
    };

    const stage_HiddenSingle = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numHiddenSingle++;
      const startTime = performance.now();
      let didSomething = false;

      stepCount++;
      const rowCandidateCounts = NINE.map(n => {
        const cCounts = Array(9).fill(0);
        indexesOfRow(n).forEach(rc => {
          if (cells[rc].value === 0) {
            cells[rc].candidates.forEach(d => {
              cCounts[d-1] += 1;
            });
          }
        });
        return cCounts;
      });
      rowCandidateCounts.forEach((r, ri) => {
        r.forEach((c, ci) => {
          if (c === 1) {
            DO_LOGGING && s_log(`hidden single of ${ci+1} in row ${ri+1}`);
            indexesOfRow(ri+1).filter(f => cells[f].candidates.includes(ci+1)).forEach(i => {
              didSomething = true;
              cells[i].value = ci+1;
              cells[i].candidates = [];
              reduceCandidatesForPeers(cells, i);
            });
          }
        });
      });
      const colCandidateCounts = NINE.map(n => {
        const cCounts = Array(9).fill(0);
        indexesOfColumn(n).forEach(rc => {
          if (cells[rc].value === 0) {
            cells[rc].candidates.forEach(d => {
              cCounts[d-1] += 1;
            });
          }
        });
        return cCounts;
      });
      colCandidateCounts.forEach((r, ri) => {
        r.forEach((c, ci) => {
          if (c === 1) {
            DO_LOGGING && s_log(`hidden single of ${ci+1} in col ${ri+1}`);
            indexesOfColumn(ri+1).filter(f => cells[f].candidates.includes(ci+1)).forEach(i => {
              didSomething = true;
              cells[i].value = ci+1;
              cells[i].candidates = [];
              reduceCandidatesForPeers(cells, i);
            });
          }
        });
      });
      const boxCandidateCounts = NINE.map(n => {
        const cCounts = Array(9).fill(0);
        indexesOfBox(n).forEach(rc => {
          if (cells[rc].value === 0) {
            cells[rc].candidates.forEach(d => {
              cCounts[d-1] += 1;
            });
          }
        });
        return cCounts;
      });
      boxCandidateCounts.forEach((r, ri) => {
        r.forEach((c, ci) => {
          if (c === 1) {
            DO_LOGGING && s_log(`hidden single of ${ci+1} in box ${ri+1}`);
            indexesOfBox(ri+1).filter(f => cells[f].candidates.includes(ci+1)).forEach(i => {
              didSomething = true;
              cells[i].value = ci+1;
              cells[i].candidates = [];
              reduceCandidatesForPeers(cells, i);
            });
          }
        });
      });

      solutionStatistics.timeHiddenSingle += (performance.now() - startTime);
      return didSomething;
    };

    const stage_NakedDouble = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numNakedDouble++;
      const startTime = performance.now();
      let didSomething = false;
      stepCount++;
      solutionStatistics.timeNakedDouble += (performance.now() - startTime);
      return didSomething && !hasEmptyCandidates(cells);
    };

    const stage_HiddenDouble = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numHiddenDouble++;
      const startTime = performance.now();
      let didSomething = false;
      stepCount++;
      solutionStatistics.timeHiddenDouble += (performance.now() - startTime);
      return didSomething && !hasEmptyCandidates(cells);
    };

    const stage_NakedTriple = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numNakedTriple++;
      const startTime = performance.now();
      let didSomething = false;
      stepCount++;
      solutionStatistics.timeNakedTriple += (performance.now() - startTime);
      return didSomething && !hasEmptyCandidates(cells);
    };

    const stage_HiddenTriple = (cells: PuzzleCell[]): boolean => {
      solutionStatistics.numHiddenTriple++;
      const startTime = performance.now();
      let didSomething = false;
      stepCount++;
      solutionStatistics.timeHiddenTriple += (performance.now() - startTime);
      return didSomething && !hasEmptyCandidates(cells);
    };

    const stage_Guess = (cells: PuzzleCell[]): SolutionResult[] => {
      numGuess++;
      setAllCandidates(cells);
      const candidates = getCandidateIndexes(cells);
      if (candidates.length > 0) {
        let startTime = performance.now();
        const cands: number[] = [ ...cells[candidates[0]].candidates ];
        cells[candidates[0]].candidates = [];

        let solutions: SolutionResult[] = [];
        timeGuess += (performance.now() - startTime);
        cands.forEach(cand => {
          stepCount++;
          const innerCells = s_copyCells(cells);
          innerCells[candidates[0]].value = cand;
          DO_LOGGING && s_log(`guess of ${cand} in r${innerCells[candidates[0]].row}c${innerCells[candidates[0]].col}`);
          reduceCandidatesForPeers(innerCells, candidates[0]);
          if (!hasEmptyCandidates(innerCells)) {
            solutions = [ ...solutions, ...solve(innerCells) ];
          }
        });
        return solutions;
      }
      return [];
    };

    const solve = (cells: PuzzleCell[], solutions: SolutionResult[] = []): SolutionResult[] => {
      if (s_isFilled(cells)) {
        if (s_isSolved(cells)) {
          return [ ...solutions, cells.map(c => c.value).join('')];
        }
        return [];
      }

      let madeProgress = true;
      let stepsLeft = 100; // prevent craziness

      // logic based attempts,
      // continue while it still makes progress
      while(madeProgress && stepsLeft > 0) {
        // loop through all logic solutions 'OR'd together
        DO_LOGGING && s_log(`running stages`);
        madeProgress = 
          (solutionConfig.doNakedSingles && stage_NakedSingle(cells)) ||
          (solutionConfig.doHiddenSingles && stage_HiddenSingle(cells)) ||
          (solutionConfig.doHiddenSingles && stage_NakedDouble(cells)) ||
          (solutionConfig.doHiddenSingles && stage_HiddenDouble(cells)) ||
          (solutionConfig.doHiddenSingles && stage_NakedTriple(cells)) ||
          (solutionConfig.doHiddenSingles && stage_HiddenTriple(cells));
        stepsLeft--;
      }
      DO_LOGGING && s_log(`stages failed to do anything`);
      // DO_LOGGING && s_log(`http://192.168.0.37:3000/puzzle/${cells.map(c => c.value || 0).join('')}`);
      // DO_LOGGING && s_log(Puzzle.outputTextPuzzle(cells.map(c => c.value || 0).join('')));

      if (hasEmptyCandidates(cells)) {
        return [];
      }

      stepCount++;
      // return [];

      if (!s_isFilled(cells)) {
        return stage_Guess(cells);
      } else if (stepCount < 200000) {
        return solve(cells, solutions);
      } else {
        return [];
      }
    };

    setAllCandidates(solutionCells);
    const solutions = solve(solutionCells);

    const endTime = performance.now();
    DO_FINAL_LOGGING && console.log(`sol5 took ${stepCount} steps in ${endTime - startTime}ms: [${solutions.join(',')}]`);
    DO_TABLE_LOGGING && console.table({
      ...solutionStatistics,
      numGuess,
      timeGuess,
      numCopyCells,
      timeCopyCells,
      numSetAllCandidates,
      timeSetAllCandidates,
      numGetCandidateIndexes,
      timeGetCandidateIndexes,
      numReduceCandidatesForPeers,
      timeReduceCandidatesForPeers,
      numHasEmptyCandidates,
      timeHasEmptyCandidates,
    });

    switch (solutions.length) {
      case 0:
        return { ...EmptySolutionOutput, solution: 'no solution', totalTime: (endTime - startTime) };
      case 1:
        return { ...EmptySolutionOutput, solution: solutions[0], totalTime: (endTime - startTime) };
      default:
        return { ...EmptySolutionOutput, solution: 'multiple solutions', multipleSolutions: solutions, totalTime: (endTime - startTime) };
    }
  };
}
