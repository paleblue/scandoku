import { iSolver } from '@solvers';
import { Puzzle, cellLocationFromIndex, SolutionConfig, SolutionOutput, EmptySolutionOutput, SolutionConfig_NoLogging } from '@types';

export class Atlanta implements iSolver {
  public getSolution = async (puzzle: String, _solutionConfig: SolutionConfig = SolutionConfig_NoLogging ): Promise<SolutionOutput> => {
    type valpair = { value: number, fixed: boolean, candidateIndex: number };
    const cellValues: number[] = Array.from(puzzle).map(s => parseInt(s));
    if (cellValues.includes(0) === false) return { ...EmptySolutionOutput, solution: cellValues.join('') };
    const cellValuesStr: string = cellValues.join('');
    if (!Puzzle.isValid(cellValuesStr)) return { ...EmptySolutionOutput, solution: 'invalid' };

    const cells: valpair[] = cellValues.map<valpair>(c => {
      return { value: c, fixed: true, candidateIndex: 0 };
    });
    
    const unsolvedIndexes = cellValues.map((c, i) => c !== 0 ? -1 : i).filter(v => v > -1);
    let unsolvedCandidates: string[] = unsolvedIndexes.map(ui => Puzzle.getCandidates(cellValuesStr, cellLocationFromIndex(ui).row, cellLocationFromIndex(ui).col).join(''));

    // if any cells have zero candidates, it has no solution
    if (unsolvedCandidates.some(s => s === '')) return { ...EmptySolutionOutput, solution: 'no solution' };

    // rectify any singles just to get that out of the way
    for (let ui = 0; ui < unsolvedCandidates.length; ui++) {
      if (unsolvedCandidates[ui].length === 1) {
        cells[unsolvedIndexes[ui]] = { value: parseInt(unsolvedCandidates[ui]), fixed: true, candidateIndex: 0 };
      }
    }
    let running: boolean = true;
    let filled: number[] = Array(unsolvedIndexes.length).fill(0);

    const possibleSolutions: string[] = [];
    let maxLoops: number = 100;

    let uIndex: number = 0;
    let indexComplete: boolean[] = Array(unsolvedIndexes.length).fill(false);
    // console.log('unsolvedIndexes / candidates', unsolvedIndexes, unsolvedCandidates, cellValues.join(''));
    while (running && maxLoops > 0) {
      if (uIndex < unsolvedIndexes.length) {
        // console.log('uIndex / index', uIndex, unsolvedIndexes[uIndex], unsolvedCandidates[uIndex], cells[unsolvedIndexes[uIndex]].candidateIndex);
        if (cells[unsolvedIndexes[uIndex]].candidateIndex < unsolvedCandidates[uIndex].length) {
          unsolvedCandidates = unsolvedIndexes.map(ui => Puzzle.getCandidates(cellValuesStr, cellLocationFromIndex(ui).row, cellLocationFromIndex(ui).col).join(''));
          cells[unsolvedIndexes[uIndex]] = {
            value: parseInt(unsolvedCandidates[uIndex].substr(cells[unsolvedIndexes[uIndex]].candidateIndex, 1)),
            fixed: false,
            candidateIndex: cells[unsolvedIndexes[uIndex]].candidateIndex + 1
          };
          filled[uIndex] = cells[unsolvedIndexes[uIndex]].value;
          if (filled.every(f => f > 0)) {
            const testresult = Puzzle.isSolved(cells.map(c => c.value).join(''));
            if (testresult) {
              possibleSolutions.push(cells.map(c => c.value).join(''));
            }
          }
          uIndex += 1;
        } else {
          indexComplete[uIndex] = true;
        }
      } else {
        // console.log('working down');
        uIndex -= 1;
        if (cells[unsolvedIndexes[uIndex]].candidateIndex === unsolvedCandidates[uIndex].length) {
          while (uIndex >= 0 && cells[unsolvedIndexes[uIndex]].candidateIndex === unsolvedCandidates[uIndex].length) {
            // console.log('reached final candidate for', uIndex);
            indexComplete[uIndex] = false;
            filled[uIndex] = 0;
            cells[unsolvedIndexes[uIndex]].candidateIndex = 0;
            uIndex -= 1;
            // console.log('reverted candidate index', uIndex+1, 'to 0, uIndex to', uIndex);
          }
          if (uIndex < 0) {
            // exhausted all options
            running = false;
          }
        }
      }
      maxLoops -= 1;
    }

    if (possibleSolutions.length > 1) {
      return { ...EmptySolutionOutput, solution: 'multiple solutions', multipleSolutions: possibleSolutions };
    } else if (possibleSolutions.length === 0) {
      return { ...EmptySolutionOutput, solution: 'no solution' };
    } else {
      return { ...EmptySolutionOutput, solution: possibleSolutions[0] };
    }
  }
}
