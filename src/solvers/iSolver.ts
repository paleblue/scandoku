import { SolutionConfig, SolutionOutput, SolutionPacket } from '@types';

export interface iSolver {
  getSolution(puzzle: string, config: SolutionConfig): Promise<SolutionOutput>
}

export interface iSolverStage {
   stage(packet: SolutionPacket, parent: iSolver): boolean;
}
