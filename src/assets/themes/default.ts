import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    mode: "dark",
    background: {
      paper: "#111C33",
      default: "#111C33",
    },
    text: {
      primary: "#ffffff",
    },
    primary: {
      main: "#111c33",
      light: "#40495b",
      dark: "#0b1323",
      contrastText: "#ffffff",
    },
    secondary: {
      main: "#21604c",
      light: "#4d7f6f",
      dark: "#174335",
      contrastText: "#ffffff",
    },
    icons: {
      easy: "#26B32C",
      medium: "#F7E44B",
      hard: "#FB983F",
      extreme: "#EF1F1F",
    },
    puzzle: {
			primaryHighlight: "#4F72B8",
			secondaryHighlight: "#364F82",
			primaryHighlightArea: "#233E42",
			secondaryHighlightArea: "#283550",
			unsetHighlight: "#2e6256",
			possibleHighlight: "#111122",
			filledHighlight: "#242E43",
      additionalPrimaryHighlight: "#6d2d7c",
      additionalSecondaryHighlight: "#35173c",
			bigDigit: "#D6E4FF",
			fixedDigit: "#FFFFFF",
			gridThick: "#4F72B8",
			gridThin: "#4d6087",
      warning: "#ff0000",
      warningDark: "#990000",
      warningDark2: "#440000",
    },
  },
  components: {
    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundColor: "var(--color-background)",
        },
      },
    },
    MuiRadio: {
      defaultProps: {
        color: "secondary",
      },
    },
  },
});

export default theme;
