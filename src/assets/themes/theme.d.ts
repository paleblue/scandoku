import { Palette, PaletteOptions } from '@mui/material/styles'; // eslint-disable-line  @typescript-eslint/no-unused-vars

type PuzzleColors = {
  primaryHighlight: string;
  secondaryHighlight: string;
  primaryHighlightArea: string;
  secondaryHighlightArea: string;
  unsetHighlight: string;
  possibleHighlight: string;
  filledHighlight: string;
  additionalPrimaryHighlight: string;
  additionalSecondaryHighlight: string;
	bigDigit: string;
	fixedDigit: string;
	gridThick: string;
	gridThin: string;
  warning: string;
  warningDark: string;
  warningDark2: string;
};

type IconColors = {
  easy: string;
  medium: string;
  hard: string;
  extreme: string;
};

declare module '@mui/material/styles' {
  interface Palette {
    puzzle?: PuzzleColors,
    icons?: IconColors,
  }
	interface PaletteOptions {
    puzzle?: PuzzleColors,
    icons?: IconColors,
	}
};
