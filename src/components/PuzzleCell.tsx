import React, { FC, ReactElement, useEffect, useCallback, useState } from 'react';
import { PencilNumber, BigNumber } from '@components';
import { useCell, useHighlights } from '@hooks';
import { CellLocation, NullLocation, isNullLocation } from '@types';
import { usePuzzleState, usePlayConfigState, useMouseEventState } from '@context';
import classNames from 'classnames';
import { styled, Theme } from '@mui/system';
import { Box, BoxProps } from '@mui/material';
import '@scss/PuzzleCell.scss';

type CellColorVariants = 'default' | 'primary-highlight' | 'primary-highlight-area' | 'secondary-highlight' | 'secondary-highlight-area' | 'unset' | 'error' | 'primary-error' | 'secondary-error' | 'possible-highlight' | 'filled-highlight' | 'additional-primary-highlight' | 'additional-secondary-highlight';

type PuzzleCellProps = {
  className?: string,
  row: number,
  col: number,
};

type PuzzleCellDivProps = {
  variant: CellColorVariants,
};

const getCellColor = (theme: Theme, variant: CellColorVariants, defaultColor = 'transparent'): string => {
  switch (variant) {
    case 'error':
      return theme.palette.puzzle?.warningDark || defaultColor;
    case 'primary-error':
      return theme.palette.puzzle?.warning || defaultColor;
    case 'secondary-error':
      return theme.palette.puzzle?.warningDark2 || defaultColor;
    case 'primary-highlight':
      return theme.palette.puzzle?.primaryHighlight || defaultColor;
    case 'primary-highlight-area':
      return theme.palette.puzzle?.primaryHighlightArea || defaultColor;
    case 'secondary-highlight':
      return theme.palette.puzzle?.secondaryHighlight || defaultColor;
    case 'secondary-highlight-area':
      return theme.palette.puzzle?.secondaryHighlightArea || defaultColor;
    case 'unset':
      return theme.palette.puzzle?.unsetHighlight || defaultColor;
    case 'possible-highlight':
      return theme.palette.puzzle?.possibleHighlight || defaultColor;
    case 'filled-highlight':
      return theme.palette.puzzle?.filledHighlight || defaultColor;
    case 'additional-primary-highlight':
      return theme.palette.puzzle?.additionalPrimaryHighlight || defaultColor;
    case 'additional-secondary-highlight':
      return theme.palette.puzzle?.additionalSecondaryHighlight || defaultColor;
    case 'default':
    default:
      return defaultColor;
  };
};

const Cell = styled(
  (props: PuzzleCellDivProps & BoxProps) => <Box {...props} />, {
    shouldForwardProp: (prop) => prop !== 'variant' && prop !== 'error' && prop !== 'fixedDigit',
    name: 'PuzzleCellDiv',
    slot: 'Root',
  })(({ theme, variant }) => ({
    borderRight: `solid 1px ${theme.palette.puzzle?.gridThin}`,
    borderBottom: `solid 1px ${theme.palette.puzzle?.gridThin}`,
    backgroundColor: getCellColor(theme, variant),
  }));

const PuzzleCell: FC<PuzzleCellProps> = ({ row, col, className = '' }):ReactElement => {
  const [ classes, setClasses ] = useState(classNames([ 'puzzle-cell', className ]));
  const { cell, toggleCellPencil, toggleSelectCell, toggleMultiSelectCell, addMultiSelectCell } = useCell(row, col);
  const { isPrimaryHighlight, isMultiSelectHighlight, isPrimaryAreaHighlight, isSecondaryHighlight, isSecondaryAreaHighlight, existsForCell, existsForPencil } = useHighlights();
  const [ selectedValue, setSelectedValue ] = useState<number>(0);
  const [ selectedCell, setSelectedCell ] = useState<CellLocation>(NullLocation);
  const [ cellColorVariant, setCellColorVariant ] = useState<CellColorVariants>('default');
  const puzzle = usePuzzleState();
  const playConfig = usePlayConfigState();
  const mouseEventState = useMouseEventState();

  const pencilClickHandler = (event: MouseEvent | React.MouseEvent, n: number) => {
    event.preventDefault();
    event.stopPropagation();
    toggleCellPencil(n);
  };

  const mouseDownHandler = (event: MouseEvent | React.MouseEvent) => {
    if (event.button === 0) {
      if (event.shiftKey || event.ctrlKey) {
        toggleMultiSelectCell();
      } else {
        toggleSelectCell();
      }
    }
  };

  const dragOverHandler = useCallback((event: MouseEvent | React.MouseEvent) => {
    if (event.buttons === 1 && mouseEventState.isDragging) {
      addMultiSelectCell();
    }
  }, [mouseEventState, addMultiSelectCell]);

  useEffect(() => {
    const c = [ 'puzzle-cell', { 'pencil-centered': cell.pencilMode === 'center' }, { 'pencil-corners': (cell.pencilMode === 'corners' || cell.pencilMode === undefined) }, className];
    if (cell.value && existsForCell(row, col, cell.value)) {
      if (cell.fixed) {
        if (!isNullLocation(selectedCell) && selectedCell.row === row && selectedCell.col === col) {
          setCellColorVariant('error'); // fixed and selected
          c.push('error');
        } else {
          setCellColorVariant('secondary-error'); // fixed and not selected
          c.push('secondary-error');
        }
      } else {
        setCellColorVariant('primary-error'); // not fixed
        c.push('primary-error');
      }
    } else {
      if (playConfig.additionalHighlightMode === 'singles' && !cell.value && cell.pencilValues.length === 1) {
        if (isPrimaryHighlight(row, col)) {
          setCellColorVariant('additional-primary-highlight');
        } else {
          setCellColorVariant('additional-secondary-highlight');
        }
        c.push('singles-highlight');
      } else if (playConfig.additionalHighlightMode === 'binary' && !cell.value && cell.pencilValues.length === 2) {
        if (isPrimaryHighlight(row, col)) {
          setCellColorVariant('additional-primary-highlight');
        } else {
          setCellColorVariant('additional-secondary-highlight');
        }
        c.push('binary-highlight');
      } else if (isPrimaryHighlight(row, col)) {
        setCellColorVariant('primary-highlight');
        c.push('primary-highlight');
      } else if (isMultiSelectHighlight(row, col)) {
        setCellColorVariant('secondary-highlight');
        c.push('secondary-highlight');
      } else if (isPrimaryAreaHighlight(row, col)) {
        setCellColorVariant('primary-highlight-area');
        c.push('primary-highlight-area');
      } else if (isSecondaryHighlight(row, col)) {
        setCellColorVariant('secondary-highlight');
        c.push('secondary-highlight');
      } else if (isSecondaryAreaHighlight(row, col)) {
        setCellColorVariant('secondary-highlight-area');
        c.push('secondary-highlight-area');
      } else if (!cell?.value && selectedValue && cell?.pencilValues.includes(selectedValue)) {
        setCellColorVariant('unset');
        c.push('unset');
      } else if (selectedValue) {
        if (!cell?.value) {
          setCellColorVariant('possible-highlight');
          c.push('possible-highlight');
        } else {
          setCellColorVariant('filled-highlight');
          c.push('filled-highlight');
        }
      } else {
        setCellColorVariant('default');
        c.push('default');
      }
    }
    setClasses(classNames(c));
  }, [isPrimaryHighlight, isMultiSelectHighlight, isPrimaryAreaHighlight, isSecondaryHighlight, isSecondaryAreaHighlight, className, row, col, setClasses, cell, selectedValue, selectedCell, existsForCell, setCellColorVariant, playConfig]);

  useEffect(() => {
    setSelectedCell(puzzle.selectedCell || NullLocation);
    setSelectedValue(puzzle.selectedValue || 0);
  }, [puzzle]);

  const renderBigNumber = (fixed: boolean, value: number): ReactElement => {
    return (
      <BigNumber 
        fixed={fixed} 
        value={value} 
        error={!fixed && existsForCell(row, col, value)}
      />
    );
  };

  const renderPencilNumbers = (): ReactElement => {
    if (cell.pencilMode === 'center') {
      return (
        <>
          { cell.pencilValues.sort().map(i => {
            return <PencilNumber 
                      key={i} 
                      value={i} 
                      centered={true}
                      selected={true} 
                      highlighted={cell.pencilValues.includes(i) && (selectedValue === i)}
                      error={cell.pencilValues.includes(i) ? existsForPencil(row, col, i) : false}
                   />
          })}
        </>
      );
    }
    return (
      <>
        { [1,2,3,4,5,6,7,8,9].map(i => {
          return <PencilNumber 
                    key={i} 
                    value={i} 
                    selected={cell.pencilValues.includes(i)} 
                    highlighted={cell.pencilValues.includes(i) && (selectedValue === i)}
                    error={cell.pencilValues.includes(i) ? existsForPencil(row, col, i) : false}
                    onClick={playConfig.cellClickMode === 'pencil' ? pencilClickHandler : undefined} 
                 />
        })}
      </>
    );
  };

  return cell ? (
    <>
      <Cell variant={cellColorVariant} className={classes} onMouseDown={mouseDownHandler} onMouseOver={dragOverHandler}>
        { 
          cell?.value !== undefined ? 
          renderBigNumber(cell.fixed, cell.value) : 
          renderPencilNumbers()
        }
      </Cell>
    </>
  ) : (<></>);
};

export default PuzzleCell;
