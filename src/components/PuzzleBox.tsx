import React, { FC, ReactElement } from 'react';
import { PuzzleCell } from '@components';
import { NINE, BoxLayouts } from '@constants';
import classNames from 'classnames';
import '@scss/PuzzleBox.scss';

type PuzzleBoxProps = {
  className?: string;
  boxNum: number;
};

const PuzzleBox: FC<PuzzleBoxProps> = ({ className, boxNum }):ReactElement => {
  const classes = classNames([ "puzzle-box", className ]);

  return (
    <div className={classes}>
      { NINE.map(i => {
        return (
          <PuzzleCell key={i}
              row={BoxLayouts[boxNum].rows[i-1]} 
              col={BoxLayouts[boxNum].cols[i-1]}
           />
        );
      })}
    </div>
  );
};

export default PuzzleBox;
