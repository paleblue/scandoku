import React, { FC, ReactElement } from 'react';
import { Button } from '@mui/material';

type OpenNavButtonProps = {
  [x:string]: unknown;
};

const OpenNavButton: FC<OpenNavButtonProps> = ({ ...props }): ReactElement => {
  return (
    <Button 
      sx={{
        color: 'text.primary',
        backgroundColor: 'puzzle.primaryHighlight',
        width: '100%',
        margin: '12px',
      }}
      {...props} />
  );
};

export default OpenNavButton;
