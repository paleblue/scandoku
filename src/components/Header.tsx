import React, { FC, ReactElement } from 'react';
import { Box, IconButton, Menu, MenuItem, Divider, ListItemIcon, ListItemText } from '@mui/material';
import { useHistory } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import GridOnIcon from '@mui/icons-material/GridOn';
import CreateIcon from '@mui/icons-material/Create';
import FolderIcon from '@mui/icons-material/Folder';
import DialpadIcon from '@mui/icons-material/Dialpad';
import { usePuzzleState, usePuzzleDispatch } from '@context';
import { NullLocation, Puzzle, SolutionConfig_NoLogging, SolutionConfig_AllLogging } from '@types';
import { Chicago, Eastlake, Fresno } from '@solvers';

type HeaderProps = {
  hideNav?: boolean
};

const Header: FC<HeaderProps> = ({ hideNav }): ReactElement => {
  const puzzle = usePuzzleState();
  const puzzleDispatch = usePuzzleDispatch();

  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleRestart = () => {
    puzzleDispatch({
      type: 'resetPuzzle',
      cell: NullLocation,
      payload: 0,
    });
    handleClose();
  };

  const handleFillCandidates = () => {
    puzzleDispatch({
      type: 'fillCandidates',
      cell: NullLocation,
      payload: 0,
    });
    handleClose();
  };

  const handleGetSolution3 = async () => {
    const solver = new Chicago();
    const sol = (await solver.getSolution(Puzzle.toLongForm(puzzle), SolutionConfig_NoLogging)).solution;
    console.log(sol);
    handleClose();
  };

  const handleGetSolution5 = async () => {
    const solver = new Eastlake();
    const sol = (await solver.getSolution(Puzzle.toLongForm(puzzle), SolutionConfig_NoLogging)).solution;
    console.log(sol);
    handleClose();
  };

  const handleGetSolution6 = async () => {
    const solver = new Fresno();
    const sol = (await solver.getSolution(Puzzle.toLongForm(puzzle), SolutionConfig_AllLogging)).solution;
    console.log(sol);
    handleClose();
  };

  const handleCreatePuzzle = async () => {
    const output = 'disconnected'; // await Puzzle.createValidPuzzle();
    console.log(output);
    handleClose();
  };

  return (
    <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '6px',
        marginBottom: '6px',
      }}>
      { !hideNav && (
        <IconButton 
          onClick={() => history.push('/')}
          sx={{
            backgroundColor: 'primary.dark',
            borderRadius: '2px',
          }}
        >
          <HomeIcon />
        </IconButton> 
      )}
      <header className="App-header">Scandoku</header>
      { !hideNav && (
        <>
          <IconButton 
            sx={{
              backgroundColor: 'primary.dark',
              borderRadius: '2px',
            }}
            aria-label="menu"
            id="header-menu-button"
            aria-controls="header-menu"
            aria-expanded={open ? 'true' : undefined}
            aria-haspopup="true"
            onClick={handleClick}
          >
            <MenuIcon />
          </IconButton> 
          <Menu
            id="header-menu"
            MenuListProps={{
              'aria-labelledby': 'header-menu-button',
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={handleRestart}>
              <ListItemIcon>
                <SettingsBackupRestoreIcon />
              </ListItemIcon>
              <ListItemText>
                Restart Puzzle
              </ListItemText>
            </MenuItem>
            <MenuItem onClick={handleFillCandidates}>
              <ListItemIcon>
                <DialpadIcon />
              </ListItemIcon>
              <ListItemText>
                Fill Candidates
              </ListItemText>
            </MenuItem>
            <MenuItem onClick={handleGetSolution3}>
              <ListItemIcon />
              <ListItemText>
                Get Solution (Chicago - sol3)
              </ListItemText>
            </MenuItem>
            <MenuItem onClick={handleGetSolution5}>
              <ListItemIcon />
              <ListItemText>
                Get Solution (Eastlake - sol5)
              </ListItemText>
            </MenuItem>
            <MenuItem onClick={handleGetSolution6}>
              <ListItemIcon />
              <ListItemText>
                Get Solution (Fresno - sol6)
              </ListItemText>
            </MenuItem>
            <Divider />
            <MenuItem onClick={handleCreatePuzzle}>
              <ListItemIcon>
                <GridOnIcon sx={{ color: 'icons.easy' }} />
              </ListItemIcon>
              <ListItemText>Easy</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => history.push('/') }>
              <ListItemIcon>
                <GridOnIcon sx={{ color: 'icons.medium' }} />
              </ListItemIcon>
              <ListItemText>Medium</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => history.push('/') }>
              <ListItemIcon>
                <GridOnIcon sx={{ color: 'icons.hard' }} />
              </ListItemIcon>
              <ListItemText>Hard</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => history.push('/') }>
              <ListItemIcon>
                <GridOnIcon sx={{ color: 'icons.extreme' }} />
              </ListItemIcon>
              <ListItemText>Extreme</ListItemText>
            </MenuItem>
            <Divider />
            <MenuItem onClick={() => history.push('/') }>
              <ListItemIcon>
                <FolderIcon />
              </ListItemIcon>
              <ListItemText>Load Previous</ListItemText>
            </MenuItem>
            <MenuItem onClick={() => history.push('/') }>
              <ListItemIcon>
                <CreateIcon />
              </ListItemIcon>
              <ListItemText>Create Puzzle</ListItemText>
            </MenuItem>
          </Menu>
        </>
      )}
    </Box>
  );
};

export default Header;
