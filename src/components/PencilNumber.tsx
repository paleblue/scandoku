import React, { FC, ReactElement, MouseEvent } from 'react';
import classNames from 'classnames';

type PencilNumberProps = {
  value: number,
  selected?: boolean,
  highlighted?: boolean,
  error?: boolean,
  onClick?: (event: MouseEvent, n: number) => void,
  centered?: boolean,
};

const PencilNumber: FC<PencilNumberProps> = ({ value, selected, highlighted, error, onClick, centered = false }):ReactElement => {
  const classes = classNames('pencil-number', { 'selected': !!selected }, { 'highlighted': !!highlighted}, { 'error': !!error }, { 'clickable': !!onClick }, { 'centered': !!centered });

  const clickHandler = (event: MouseEvent) => {
    if (onClick) {
      onClick(event, value);
    }
  };
  return (
    <div className={classes} onClick={clickHandler}>{value}</div>
  );
};

export default PencilNumber;
