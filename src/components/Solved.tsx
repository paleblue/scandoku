import React, { FC, ReactElement } from 'react';
import { Box } from '@mui/material';

type SolvedProps = {
  visible: boolean
};

const Solved: FC<SolvedProps> = ({ visible }): ReactElement => {
  return (
    <Box className={`solved ${visible ? '' : 'hidden'}`}>Solved</Box> 
  );
};

export default Solved;
