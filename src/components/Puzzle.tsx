import React, { FC, ReactElement } from 'react';
import { PuzzleBox } from '@components';
import classNames from 'classnames';
import { useMouseEventState, useMouseEventDispatch } from '@context';
import { useAvailableSpots } from '@hooks';
import { NINE } from '@constants';
import { Paper } from '@mui/material';

type PuzzleProps = {
  className?: string;
};

const Puzzle: FC<PuzzleProps> = ({ className  }):ReactElement => {
  const classes = classNames([ "puzzle", className ]);
  const { availableInRows, availableInCols } = useAvailableSpots();
  const mouseEventState = useMouseEventState();
  const mouseEventDispatch = useMouseEventDispatch();

  const dragStartHandler = (event: MouseEvent | React.MouseEvent) => {
    if (!mouseEventState.isDragging) {
      mouseEventDispatch({
        type: 'startDragging',
        event: event as MouseEvent
      });
    }
  };

  const dragEndHandler = (event: MouseEvent | React.MouseEvent) => {
    if (mouseEventState.isDragging) {
      mouseEventDispatch({
        type: 'endDragging',
        event: event as MouseEvent
      });
    }
  };

  return (
    <Paper className="puzzle-shell" elevation={3} onMouseDown={dragStartHandler} onMouseUp={dragEndHandler} onMouseLeave={dragEndHandler}>
      <div className="puzzlerow-horiz puzzlerow-top">
        <div className="corner">&middot;</div>
        <div className="puzzle-annotation col-markers">
          { NINE.map(i => {
            return <div key={i}>{i}</div>;
          })}
        </div>
        <div className="corner">&middot;</div>
      </div>
      <div className="puzzlerow-middle">
        <div className="puzzle-annotation puzzlerow-vert row-markers">
          { NINE.map(i => {
            return <div key={i}>{i}</div>;
          })}
        </div>
        <div id="puzzle" className={classes}>
          { NINE.map(i => {
            return <PuzzleBox key={i} boxNum={i} />;
          })}
        </div>
        <div className="puzzle-annotation puzzlerow-vert row-available">
          { availableInRows.map((i, index) => {
            const classes = i ? '' : 'null';
            return <div key={index} className={classes}>{i || '-'}</div>;
          })}
        </div>
      </div>
      <div className="puzzlerow-horiz puzzlerow-bottom">
        <div className="corner">&middot;</div>
        <div className="puzzle-annotation col-available">
          { availableInCols.map((i, index) => {
            const classes = i ? '' : 'null';
            return <div key={index} className={classes}>{i || '-'}</div>;
          })}
        </div>
        <div className="corner">&middot;</div>
      </div>
    </Paper>
  );
};

export default Puzzle;
