import React, { FC, ReactElement, MouseEvent } from 'react';
import { styled } from '@mui/system';
import { Box, BoxProps } from '@mui/material';
import classNames from 'classnames';

type BigNumberProps = {
  value: number,
  fixed?: boolean,
  error?: boolean,
  onClick?: (event: MouseEvent) => void,
};

type BigNumberDivProps = {
  fixed?: boolean,
};

const Div = styled(
  (props: BigNumberDivProps & BoxProps) => <Box {...props} />, {
    shouldForwardProp: (prop) => prop !== 'error' && prop !== 'fixed',
    name: 'BigNumberDiv',
    slot: 'Root',
  })(({ theme, fixed }) => ({
    fontSize: '42px',
    lineHeight: '54px',
    position: 'relative',
    top: '-2px',
    color: fixed ? theme.palette.puzzle.fixedDigit : theme.palette.puzzle.bigDigit,
    textShadow: fixed ? '2px 1px 3px #666' : 'none',
  }));

const BigNumber: FC<BigNumberProps> = ({ value, error, fixed, onClick }):ReactElement => {
  const clickHandler = (event: MouseEvent) => {
    if (onClick) {
      onClick(event);
    }
  };
  const classes = classNames(['big-number', { 'error': error }]);

  return (
    <Div className={classes} onClick={clickHandler} fixed={fixed}>{value}</Div>
  );
};

export default BigNumber;
