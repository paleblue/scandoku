import React, { FC, ReactElement, MouseEvent, ChangeEvent, useEffect, useState } from 'react';
import { useAvailableSpots, useRemaining } from '@hooks';
import { usePuzzleState, usePuzzleDispatch, usePlayConfigState, usePlayConfigDispatch } from '@context';
import { CellControlMode, CellClickMode, AdditionalHighlightMode, NullLocation } from '@types';
import { NINE, EIGHT } from '@constants';
import { FormControlLabel, RadioGroup, Radio } from '@mui/material';
import classNames from 'classnames';
import '@scss/PuzzleCellControls.scss';

type PuzzleCellControlProps = {
  className?: string,
};

const PuzzleCellControls: FC<PuzzleCellControlProps> = ({ className = '' }):ReactElement => {
  const classes = classNames([ 'puzzle-cell-controls', className ]);
  const [ selectedValue, setSelectedValue ] = useState<number>(0);
  const { numsLeft } = useRemaining();
  const { availableInBoxes } = useAvailableSpots();
  const playConfig = usePlayConfigState();
  const playConfigDispatch = usePlayConfigDispatch();
  const puzzle = usePuzzleState();
  const dispatch = usePuzzleDispatch();

  const cellControlClickHandler = (_event: MouseEvent, n: number) => {
    switch (playConfig.cellControlMode) {
      case 'big': {
        dispatch({
          type: 'setCellValue',
          cell: puzzle.selectedCell || NullLocation,
          payload: n,
        });
        break;
      }
      case 'pencil': {
        dispatch({
          type: 'toggleCellPencil',
          cell: puzzle.selectedCell || NullLocation,
          payload: n,
        });
        break;
      }
      default: {
        dispatch({
          type: 'toggleSelectValue',
          cell: NullLocation,
          payload: n,
        });
        break;
      }
    }
  };

  const cellControlModeChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    playConfigDispatch({
      type: 'cellControlMode',
      value: event?.target?.value as CellControlMode,
    });
  };

  const cellClickModeChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    playConfigDispatch({
      type: 'cellClickMode',
      value: event?.target?.value as CellClickMode,
    });
  };

  const additionalHighlightModeChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    playConfigDispatch({
      type: 'additionalHighlightMode',
      value: event?.target?.value as AdditionalHighlightMode,
    });
  };

  useEffect(() => {
    setSelectedValue(puzzle.selectedValue || 0);
  }, [puzzle]);

  return (
    <div className={classes}>
      <div className="number-control-boxes">
        <div className="available-boxes">
          <>
            { EIGHT.map(i => {
              return <div key={i} className="available-boxes-cell">{availableInBoxes[i] || ''}</div>
            })}
          </>
        </div>
        { NINE.map(i => {
          const cellClasses = classNames(['number-cell', { 'selected': selectedValue === i }, { 'completed': numsLeft[i-1] === 0 }]);
          return <div 
                    className={cellClasses}
                    key={i} 
                    onClick={(event) => cellControlClickHandler(event, i)} 
                  >
                    <div className="number-cell-value">{i}</div>
                    <div className="number-cell-left">{numsLeft[i-1] || ''}</div>
                  </div>
        })}
      </div>
      <div className="radio-group number-control-mode">
        <RadioGroup name="controlMode" aria-label="controlMode" value={playConfig.cellControlMode} onChange={cellControlModeChangeHandler} row>
          <FormControlLabel label="highlight" value="highlight" control={<Radio />} />
          <FormControlLabel label="big number" value="big" control={<Radio />} />
          <FormControlLabel label="pencil mark" value="pencil" control={<Radio />} />
        </RadioGroup>
      </div>
      <div className="radio-group click-control-mode">
        <RadioGroup name="clickMode" aria-label="clickMode" value={playConfig.cellClickMode} onChange={cellClickModeChangeHandler} row>
          <FormControlLabel label="pencil marks" value="pencil" control={<Radio />} />
          <FormControlLabel label="select box" value="select" control={<Radio />} />
        </RadioGroup>
      </div>
      <div className="radio-group additional-highlight-mode">
        <RadioGroup name="additionalHighlightMode" aria-label="additionalHighlightMode" value={playConfig.additionalHighlightMode} onChange={additionalHighlightModeChangeHandler} row>
          <FormControlLabel label="none" value="none" control={<Radio />} />
          <FormControlLabel label="single values" value="singles" control={<Radio />} />
          <FormControlLabel label="binary values" value="binary" control={<Radio />} />
        </RadioGroup>
      </div>
    </div>
  );
};

export default PuzzleCellControls;
