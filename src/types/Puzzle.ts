import { Cell, CellLocation, NullLocation, isNullLocation, cellArrayIndex, indexesOfRow, indexesOfColumn, indexesOfBox, cellBoxByIndex } from '@types';
import { Base64Chars, NINE } from '@constants';

export class Puzzle {
  public id: string = '';
  public selectedCell?: CellLocation = undefined;
  public lastSelectedCell?: CellLocation = undefined;
  public selectedValue?: number = undefined;
  public multiSelectCells?: CellLocation[] = [];
  public cells: Cell[] = [];

  constructor(strInput?: string) {
    if (strInput) {
      this.cells = Puzzle.parseString(strInput).cells;
      this.id = Puzzle.toLongForm(this);
    }
  }

  public static parseString = (input: string): Puzzle => {
    if (input.length === 81) {
      // long form
      return {
        id: input,
        cells: Array.from(input).map(c => { 
          const i: number = parseInt(c);
          return {
            value: i || undefined, 
            fixed: (i > 0), 
            pencilValues: [],
          } as Cell;
        })
      };
    } else if (input.length > 14) {
      // 14 is base for short form
      const hasValuesCode: string = input.substr(0, 14);
      const cellValuesCode: string = input.substr(14);

      const cellValuesNumbers: number[] = Array.from(cellValuesCode).map(v => Base64Chars.indexOf(v));
      const cellValuesBinary: string[] = cellValuesNumbers.map(v => v.toString(2).padStart(6,'0'));
      const cellValuesBinaryConcat: string = cellValuesBinary.reduce((s, v) => `${s}${v}`, '');

      // into 4-bit chunks
      const cellValuesChunks: string[] = [];
      Array.from(cellValuesBinaryConcat).forEach((v, i) => {
        const chunk = Math.floor(i/4);
        cellValuesChunks[chunk] = `${cellValuesChunks[chunk] || ''}${v}`;
      });
      const cellValues: number[] = cellValuesChunks.filter(v => parseInt(v) !== 0).map(v => parseInt(v,2));

      const hasValuesNumbers: number[] = Array.from(hasValuesCode).map(v => Base64Chars.indexOf(v));
      const hasValuesBinary: string[] = hasValuesNumbers.map(v => v.toString(2).padStart(6,'0'));
      const hasValuesBinaryConcat: string = hasValuesBinary.reduce((s, v) => `${s}${v}`, '').substr(0,81);

      const longForm: string = Array.from(hasValuesBinaryConcat).reduce((s, v) => {
        if (v === '0') {
          return `${s}0`;
        } else if (cellValues.length > 0) {
          return `${s}${cellValues.shift()}`;
        } else {
          // ???
          return s;
        }
      }, '');
      return Puzzle.parseString(longForm);
    }
    return EmptyPuzzle;
  };

  public static toLongForm = (puzzle: Puzzle): string => {
    const outstr: string[] = [];
    puzzle.cells.forEach(c => {
      if (c.value && c.fixed) {
        outstr.push(`${c.value}`);
      } else {
        outstr.push('0');
      }
    });
    return outstr.join('');
  };

  public static toShortForm = (puzzle: Puzzle): string => {
    // first 14 characters define which cells are populated (fixed values)
    const hasValue: boolean[] = puzzle.cells.map(c => {
      return !!(c.value && c.fixed);
    });
    // pad out to nearest x6 multiple 81 + 3 = 84 (14 6-bit chars)
    hasValue[81] = false;
    hasValue[82] = false;
    hasValue[83] = false;
    // split in 6-bit chunks
    const hasValueChunks: Array<boolean[]> = [[]];
    hasValue.forEach((v, i) => {
      const chunk = Math.floor(i/6);
      hasValueChunks[chunk] = [ ...(hasValueChunks[chunk] || []), v ];
    });
    const hasValueBytes: number[] = hasValueChunks.map(val => {
      return ((val[0] ? 1 : 0) << 5) |
        ((val[1] ? 1 : 0) << 4) |
        ((val[2] ? 1 : 0) << 3) |
        ((val[3] ? 1 : 0) << 2) |
        ((val[4] ? 1 : 0) << 1) |
        (val[5] ? 1 : 0);
    }, [0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
    const hasValueChars: string = hasValueBytes.reduce((s, n) => `${s}${Base64Chars[n]}`, ''); 

    // next ?? characters define cell values, in 4-bit chunks
    const cellValues: number[] = puzzle.cells.filter(c => !!(c.value && c.fixed)).map(c => c.value || 0);
    let cellValuesBinary: string = cellValues.reduce((s, v) => `${s}${v.toString(2).padStart(4,'0')}`, '');
    // pad out to nearest x6 multiple
    const paddedLength = ((Math.ceil(cellValuesBinary.length / 6) * 6) - cellValuesBinary.length);
    for (let i = 0; i < paddedLength; i++) {
      cellValuesBinary += '0';
    }
    // split into 6-bit chunks
    const cellValueChunks: Array<boolean[]> = [[]];
    Array.from(cellValuesBinary).forEach((b, i) => {
      const chunk = Math.floor(i/6);
      cellValueChunks[chunk] = [ ...(cellValueChunks[chunk] || []), b === '1' ];
    });
    const cellValueBytes: number[] = cellValueChunks.map(val => {
      return ((val[0] ? 1 : 0) << 5) |
        ((val[1] ? 1 : 0) << 4) |
        ((val[2] ? 1 : 0) << 3) |
        ((val[3] ? 1 : 0) << 2) |
        ((val[4] ? 1 : 0) << 1) |
        (val[5] ? 1 : 0);
    }, []);
    const cellValueChars: string = cellValueBytes.reduce((s, n) => `${s}${Base64Chars[n]}`, '');

    return `${hasValueChars}${cellValueChars}`;
  };

  public static shortToLongForm = (shortForm: string): string => {
    const p = Puzzle.parseString(shortForm);
    return Puzzle.toLongForm(p);
  };

  public static isValid = (puzzle: Puzzle | string): boolean => {
    let cells: number[] = [];
    if (typeof puzzle === 'string') {
      // if ((puzzle as string).indexOf('0') > -1) return false;
      cells = Array.from(puzzle as string).map(s => parseInt(s));
    } else {
      // if ((puzzle as Puzzle).cells.some(c => !c.value)) return false;
      cells = (puzzle as Puzzle).cells.map(c => c.value || 0);
    }

    // take values of each col/row/box, count how many 1s, 2s, etc, and make sure that none of them are greater than 1
    return (
      NINE.reduce<boolean>((prev, val) => prev && (indexesOfRow(val).map(c => cells[c]).reduce<number[]>((p,v) => p.map((n, i) => (i === v-1) ? n+1 : n), [0,0,0,0,0,0,0,0,0]).sort().reverse()[0] < 2), true) &&
      NINE.reduce<boolean>((prev, val) => prev && (indexesOfColumn(val).map(c => cells[c]).reduce<number[]>((p,v) => p.map((n, i) => (i === v-1) ? n+1 : n), [0,0,0,0,0,0,0,0,0]).sort().reverse()[0] < 2), true) &&
      NINE.reduce<boolean>((prev, val) => prev && (indexesOfBox(val).map(c => cells[c]).reduce<number[]>((p,v) => p.map((n, i) => (i === v-1) ? n+1 : n), [0,0,0,0,0,0,0,0,0]).sort().reverse()[0] < 2), true)
    );
  };

  public static isSolved = (puzzle: Puzzle | string): boolean => {
    let cells: number[] = [];
    if (typeof puzzle === 'string') {
      cells = Array.from(puzzle as string).map(s => parseInt(s));
    } else {
      cells = (puzzle as Puzzle).cells.map(c => c.value || 0);
    }
    if (cells.includes(0)) return false;

    return (
      NINE.reduce<boolean>((prev, val) => prev && (JSON.stringify(indexesOfRow(val).map(c => cells[c]).sort()) === JSON.stringify(NINE)), true) &&
      NINE.reduce<boolean>((prev, val) => prev && (JSON.stringify(indexesOfColumn(val).map(c => cells[c]).sort()) === JSON.stringify(NINE)), true) &&
      NINE.reduce<boolean>((prev, val) => prev && (JSON.stringify(indexesOfBox(val).map(c => cells[c]).sort()) === JSON.stringify(NINE)), true)
    );
  };

  public static getCandidates = (puzzle: Puzzle | string, row: number, col: number): number[] => {
    const cellValues: number[] = (typeof puzzle === 'string') ? Array.from(puzzle as string).map(s => parseInt(s)) : (puzzle as Puzzle).cells.map(c => c.value || 0);

    const index = cellArrayIndex(row, col);
    if (cellValues[index] > 0) {
      return [];
    } else {
      const valuesInRow = indexesOfRow(row).filter(c => cellValues[c]).map(c => cellValues[c]);
      const valuesInCol = indexesOfColumn(col).filter(c => cellValues[c]).map(c => cellValues[c]);
      const valuesInBox = indexesOfBox(cellBoxByIndex(index)).filter(c => cellValues[c]).map(c => cellValues[c]);
      return NINE.filter(v => !(valuesInRow.includes(v) || valuesInCol.includes(v) || valuesInBox.includes(v)));
    }
  };

  public static outputTextPuzzle = (puzzle: Puzzle | string, highlightedCell: CellLocation = NullLocation): string => {
    const p: number[] = (typeof puzzle === 'string') ? Array.from(puzzle as string).map(s => parseInt(s)) : (puzzle as Puzzle).cells.map(c => c.value || 0);
    const highlightedNumbers: string[] = Array(10).fill(0).map((_f, i) => `«${i > 0 ? i : ' '}»`);

    const joinTriad = (indexes: number[]): string => {
      const a = (!isNullLocation(highlightedCell) && cellArrayIndex(highlightedCell) === indexes[0]) ? highlightedNumbers[p[indexes[0]]] : (p[indexes[0]] > 0 ? ` ${p[indexes[0]].toString()} ` : '   ');
      const b = (!isNullLocation(highlightedCell) && cellArrayIndex(highlightedCell) === indexes[1]) ? highlightedNumbers[p[indexes[1]]] : (p[indexes[1]] > 0 ? ` ${p[indexes[1]].toString()} ` : '   ');
      const c = (!isNullLocation(highlightedCell) && cellArrayIndex(highlightedCell) === indexes[2]) ? highlightedNumbers[p[indexes[2]]] : (p[indexes[2]] > 0 ? ` ${p[indexes[2]].toString()} ` : '   ');
      return `${a}┃${b}┃${c}`;
    };

    return [`╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗`,
            `║${joinTriad([0,1,2])}║${joinTriad([3,4,5])}║${joinTriad([6,7,8])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([9,10,11])}║${joinTriad([12,13,14])}║${joinTriad([15,16,17])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([18,19,20])}║${joinTriad([21,22,23])}║${joinTriad([24,25,26])}║`,
            `╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣`,
            `║${joinTriad([27,28,29])}║${joinTriad([30,31,32])}║${joinTriad([33,34,35])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([36,37,38])}║${joinTriad([39,40,41])}║${joinTriad([42,43,44])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([45,46,47])}║${joinTriad([48,49,50])}║${joinTriad([51,52,53])}║`,
            `╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣`,
            `║${joinTriad([54,55,56])}║${joinTriad([57,58,59])}║${joinTriad([60,61,62])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([63,64,65])}║${joinTriad([66,67,68])}║${joinTriad([69,70,71])}║`,
            `╟━━━╋━━━╋━━━╫━━━╋━━━╋━━━╫━━━╋━━━╋━━━╢`,
            `║${joinTriad([72,73,74])}║${joinTriad([75,76,77])}║${joinTriad([78,79,80])}║`,
            `╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝`].join('\n');
  };

  // public static createValidPuzzle = async (): Promise<string> => {
    // let attemptsLeft: number = 1000;
    // let isValid: boolean = false;
    // let outputPuzzle: string = '000000000000000000000000000000000000000000000000000000000000000000000000000000000';
    // let failedAttempts: number = 0;

    // while (isValid === false && attemptsLeft > 0) {
      // const p: Puzzle = Puzzle.parseString('000000000000000000000000000000000000000000000000000000000000000000000000000000000');

      // // on each row, add four randomly selected digits
      // let col: number = 0;
      // let cellIndex: number = 0;
      // let candidates: number[] = [];
      // let candidateIndex: number = 0;
      // let indexAttempsLeft: number = 10;

      // NINE.forEach(row => {
        // [1,2,3,4].forEach(() => {
          // col = 0;
          // indexAttempsLeft = 10;
          // while (col === 0 && indexAttempsLeft > 0) {
            // col = Math.floor(Math.random() * 9) + 1;
            // cellIndex = cellArrayIndex(row, col);
            // if (p.cells[cellIndex].value) {
              // // already set, try again
              // col = 0;
              // indexAttempsLeft -= 1;
            // }
          // }
          // candidates = Puzzle.getCandidates(p, row, col);
          // candidateIndex = Math.floor(Math.random() * candidates.length);
          // p.cells[cellIndex].value = candidates[candidateIndex];
          // p.cells[cellIndex].fixed = true;
        // });
      // });

      // outputPuzzle = Puzzle.toLongForm(p);
      // if ((await Puzzle.getSolution(outputPuzzle)).solution.length === 81) {
        // isValid = true;
      // } else {
        // failedAttempts += 1;
      // }
      // attemptsLeft -= 1;
      // console.log('attempts left', attemptsLeft);
      // console.log('last try', outputPuzzle);
    // }

    // if (attemptsLeft < 1) {
      // console.log('fail');
      // return '';
    // } else {
      // console.log(`solution in ${failedAttempts+1} attempts: ${Puzzle.getSolution(outputPuzzle)}`);
      // console.log(Puzzle.getSolution(outputPuzzle));
      // return outputPuzzle; 
    // }
  // };
};

export type BoxLayout = {
  rows: number[],
  cols: number[],
};

export const LongSample: string = '100000000020000000003000000000400000000050000000006000000000700000000080000000009';
export const LongSolveTimePuzzle: string = '100000000508010000000200001000560190000001006916000400031006007000723810782190003';

export const EmptyPuzzle: Puzzle = {
  id: '000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  selectedCell: undefined,
  lastSelectedCell: undefined,
  selectedValue: undefined,
  cells: [
    { value: undefined, fixed: false, pencilValues: [] },         // r1c1
    { value: undefined, fixed: false, pencilValues: [] },         // r1c2
    { value: undefined, fixed: false, pencilValues: [] },         // r1c3
    { value: undefined, fixed: false, pencilValues: [] },         // r1c4
    { value: undefined, fixed: false, pencilValues: [] },         // r1c5
    { value: undefined, fixed: false, pencilValues: [] },         // r1c6
    { value: undefined, fixed: false, pencilValues: [] },         // r1c7
    { value: undefined, fixed: false, pencilValues: [] },         // r1c8
    { value: undefined, fixed: false, pencilValues: [] },         // r1c9

    { value: undefined, fixed: false, pencilValues: [] },         // r2c1
    { value: undefined, fixed: false, pencilValues: [] },         // r2c2
    { value: undefined, fixed: false, pencilValues: [] },         // r2c3
    { value: undefined, fixed: false, pencilValues: [] },         // r2c4
    { value: undefined, fixed: false, pencilValues: [] },         // r2c5
    { value: undefined, fixed: false, pencilValues: [] },         // r2c6
    { value: undefined, fixed: false, pencilValues: [] },         // r2c7
    { value: undefined, fixed: false, pencilValues: [] },         // r2c8
    { value: undefined, fixed: false, pencilValues: [] },         // r2c9

    { value: undefined, fixed: false, pencilValues: [] },         // r3c1
    { value: undefined, fixed: false, pencilValues: [] },         // r3c2
    { value: undefined, fixed: false, pencilValues: [] },         // r3c3
    { value: undefined, fixed: false, pencilValues: [] },         // r3c4
    { value: undefined, fixed: false, pencilValues: [] },         // r3c5
    { value: undefined, fixed: false, pencilValues: [] },         // r3c6
    { value: undefined, fixed: false, pencilValues: [] },         // r3c7
    { value: undefined, fixed: false, pencilValues: [] },         // r3c8
    { value: undefined, fixed: false, pencilValues: [] },         // r3c9

    { value: undefined, fixed: false, pencilValues: [] },         // r4c1
    { value: undefined, fixed: false, pencilValues: [] },         // r4c2
    { value: undefined, fixed: false, pencilValues: [] },         // r4c3
    { value: undefined, fixed: false, pencilValues: [] },         // r4c4
    { value: undefined, fixed: false, pencilValues: [] },         // r4c5
    { value: undefined, fixed: false, pencilValues: [] },         // r4c6
    { value: undefined, fixed: false, pencilValues: [] },         // r4c7
    { value: undefined, fixed: false, pencilValues: [] },         // r4c8
    { value: undefined, fixed: false, pencilValues: [] },         // r4c9

    { value: undefined, fixed: false, pencilValues: [] },         // r5c1
    { value: undefined, fixed: false, pencilValues: [] },         // r5c2
    { value: undefined, fixed: false, pencilValues: [] },         // r5c3
    { value: undefined, fixed: false, pencilValues: [] },         // r5c4
    { value: undefined, fixed: false, pencilValues: [] },         // r5c5
    { value: undefined, fixed: false, pencilValues: [] },         // r5c6
    { value: undefined, fixed: false, pencilValues: [] },         // r5c7
    { value: undefined, fixed: false, pencilValues: [] },         // r5c8
    { value: undefined, fixed: false, pencilValues: [] },         // r5c9

    { value: undefined, fixed: false, pencilValues: [] },         // r6c1
    { value: undefined, fixed: false, pencilValues: [] },         // r6c2
    { value: undefined, fixed: false, pencilValues: [] },         // r6c3
    { value: undefined, fixed: false, pencilValues: [] },         // r6c4
    { value: undefined, fixed: false, pencilValues: [] },         // r6c5
    { value: undefined, fixed: false, pencilValues: [] },         // r6c1
    { value: undefined, fixed: false, pencilValues: [] },         // r6c7
    { value: undefined, fixed: false, pencilValues: [] },         // r6c8
    { value: undefined, fixed: false, pencilValues: [] },         // r6c9

    { value: undefined, fixed: false, pencilValues: [] },         // r7c1
    { value: undefined, fixed: false, pencilValues: [] },         // r7c2
    { value: undefined, fixed: false, pencilValues: [] },         // r7c3
    { value: undefined, fixed: false, pencilValues: [] },         // r7c4
    { value: undefined, fixed: false, pencilValues: [] },         // r7c5
    { value: undefined, fixed: false, pencilValues: [] },         // r7c6
    { value: undefined, fixed: false, pencilValues: [] },         // r7c7
    { value: undefined, fixed: false, pencilValues: [] },         // r7c8
    { value: undefined, fixed: false, pencilValues: [] },         // r7c9

    { value: undefined, fixed: false, pencilValues: [] },         // r8c1
    { value: undefined, fixed: false, pencilValues: [] },         // r8c2
    { value: undefined, fixed: false, pencilValues: [] },         // r8c3
    { value: undefined, fixed: false, pencilValues: [] },         // r8c4
    { value: undefined, fixed: false, pencilValues: [] },         // r8c5
    { value: undefined, fixed: false, pencilValues: [] },         // r8c6
    { value: undefined, fixed: false, pencilValues: [] },         // r8c7
    { value: undefined, fixed: false, pencilValues: [] },         // r8c8
    { value: undefined, fixed: false, pencilValues: [] },         // r8c9

    { value: undefined, fixed: false, pencilValues: [] },         // r9c1
    { value: undefined, fixed: false, pencilValues: [] },         // r9c2
    { value: undefined, fixed: false, pencilValues: [] },         // r9c3
    { value: undefined, fixed: false, pencilValues: [] },         // r9c4
    { value: undefined, fixed: false, pencilValues: [] },         // r9c5
    { value: undefined, fixed: false, pencilValues: [] },         // r9c6
    { value: undefined, fixed: false, pencilValues: [] },         // r9c7
    { value: undefined, fixed: false, pencilValues: [] },         // r9c8
    { value: undefined, fixed: false, pencilValues: [] },         // r9c9
  ],
};

// nytimes 'easy' from 10/19/21
export const SamplePuzzle: Puzzle = {
  id: '830070006009301502206000930602080015000050629103009800070000251020530060060210090',
  selectedCell: { row: 2, col: 7 },
  lastSelectedCell: { row: 2, col: 7 },
  selectedValue: 5,
  cells: [
    { value: 8, fixed: true, pencilValues: [] },                  // r1c1
    { value: 3, fixed: true, pencilValues: [] },                  // r1c2
    { value: undefined, fixed: false, pencilValues: [1,4,5] },    // r1c3
    { value: undefined, fixed: false, pencilValues: [4,9] },      // r1c4
    { value: 7, fixed: true, pencilValues: [] },                  // r1c5
    { value: undefined, fixed: false, pencilValues: [2,4,5] },    // r1c6
    { value: undefined, fixed: false, pencilValues: [1,4] },      // r1c7
    { value: undefined, fixed: false, pencilValues: [4] },        // r1c8
    { value: 6, fixed: true, pencilValues: [] },                  // r1c9

    { value: undefined, fixed: false, pencilValues: [4,7] },      // r2c1
    { value: undefined, fixed: false, pencilValues: [4] },        // r2c2
    { value: 9, fixed: true, pencilValues: [] },                  // r2c3
    { value: 3, fixed: true, pencilValues: [] },                  // r2c4
    { value: undefined, fixed: false, pencilValues: [4,6] },      // r2c5
    { value: 1, fixed: true, pencilValues: [] },                  // r2c6
    { value: 5, fixed: true, pencilValues: [] },                  // r2c7
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r2c8
    { value: 2, fixed: true, pencilValues: [] },                  // r2c9

    { value: 2, fixed: true, pencilValues: [] },                  // r3c1
    { value: undefined, fixed: false, pencilValues: [1,4,5] },    // r3c2
    { value: 6, fixed: true, pencilValues: [] },                  // r3c3
    { value: undefined, fixed: false, pencilValues: [4,8] },      // r3c4
    { value: undefined, fixed: false, pencilValues: [4] },        // r3c5
    { value: undefined, fixed: false, pencilValues: [4,5,8] },    // r3c6
    { value: 9, fixed: true, pencilValues: [] },                  // r3c7
    { value: 3, fixed: true, pencilValues: [] },                  // r3c8
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r3c9

    { value: 6, fixed: true, pencilValues: [] },                  // r4c1
    { value: undefined, fixed: false, pencilValues: [4,9] },      // r4c2
    { value: 2, fixed: true, pencilValues: [] },                  // r4c3
    { value: undefined, fixed: false, pencilValues: [4,7] },      // r4c4
    { value: 8, fixed: true, pencilValues: [] },                  // r4c5
    { value: undefined, fixed: false, pencilValues: [3,4,7] },    // r4c6
    { value: undefined, fixed: false, pencilValues: [3,4,7] },    // r4c7
    { value: 1, fixed: true, pencilValues: [] },                  // r4c8
    { value: 5, fixed: true, pencilValues: [] },                  // r4c9

    { value: undefined, fixed: false, pencilValues: [4,7] },      // r5c1
    { value: undefined, fixed: false, pencilValues: [4,8] },      // r5c2
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r5c3
    { value: undefined, fixed: false, pencilValues: [1,4,7] },    // r5c4
    { value: 5, fixed: true, pencilValues: [] },                  // r5c5
    { value: undefined, fixed: false, pencilValues: [3,4,7] },    // r5c6
    { value: 6, fixed: true, pencilValues: [] },                  // r5c7
    { value: 2, fixed: true, pencilValues: [] },                  // r5c8
    { value: 9, fixed: true, pencilValues: [] },                  // r5c9

    { value: 1, fixed: true, pencilValues: [] },                  // r6c1
    { value: undefined, fixed: false, pencilValues: [4,5] },      // r6c2
    { value: 3, fixed: true, pencilValues: [] },                  // r6c3
    { value: undefined, fixed: false, pencilValues: [4,6,7] },    // r6c4
    { value: undefined, fixed: false, pencilValues: [2,4,6] },    // r6c5
    { value: 9, fixed: true, pencilValues: [] },                  // r6c6
    { value: 8, fixed: true, pencilValues: [] },                  // r6c7
    { value: undefined, fixed: false, pencilValues: [4,7] },      // r6c8
    { value: undefined, fixed: false, pencilValues: [4,7] },      // r6c9

    { value: undefined, fixed: false, pencilValues: [3,4,9] },    // r7c1
    { value: 7, fixed: true, pencilValues: [] },                  // r7c2
    { value: undefined, fixed: false, pencilValues: [4,8] },      // r7c3
    { value: undefined, fixed: false, pencilValues: [4,6,8,9] },  // r7c4
    { value: undefined, fixed: false, pencilValues: [4,6,9] },    // r7c5
    { value: undefined, fixed: false, pencilValues: [4,6,8] },    // r7c6
    { value: 2, fixed: true, pencilValues: [] },                  // r7c7
    { value: 5, fixed: true, pencilValues: [] },                  // r7c8
    { value: 1, fixed: true, pencilValues: [] },                  // r7c9

    { value: undefined, fixed: false, pencilValues: [4,9] },      // r8c1
    { value: 2, fixed: true, pencilValues: [] },                  // r8c2
    { value: undefined, fixed: false, pencilValues: [1,4,8] },    // r8c3
    { value: 5, fixed: true, pencilValues: [] },                  // r8c4
    { value: 3, fixed: true, pencilValues: [] },                  // r8c5
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r8c6
    { value: undefined, fixed: false, pencilValues: [4,7] },      // r8c7
    { value: 6, fixed: true, pencilValues: [] },                  // r8c8
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r8c9

    { value: undefined, fixed: false, pencilValues: [3,4,5] },    // r9c1
    { value: 6, fixed: true, pencilValues: [] },                  // r9c2
    { value: undefined, fixed: false, pencilValues: [4,5,8] },    // r9c3
    { value: 2, fixed: true, pencilValues: [] },                  // r9c4
    { value: 1, fixed: true, pencilValues: [] },                  // r9c5
    { value: undefined, fixed: false, pencilValues: [4,7,8] },    // r9c6
    { value: undefined, fixed: false, pencilValues: [3,4,7] },    // r9c7
    { value: 9, fixed: true, pencilValues: [] },                  // r9c8
    { value: undefined, fixed: false, pencilValues: [3,4,7,8] },  // r9c9
  ],
};

export const FilledPencilPuzzle: Puzzle = {
  id: '000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  selectedCell: undefined,
  lastSelectedCell: undefined,
  selectedValue: undefined,
  cells: [
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r1c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r2c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r3c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r4c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r5c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r6c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r7c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r8c9

    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c1
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c2
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c3
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c4
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c5
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c6
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c7
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c8
    { value: undefined, fixed: false, pencilValues: [1,2,3,4,5,6,7,8,9] },         // r9c9
  ],
};

export const FilledValuePuzzle: Puzzle = {
  id: '830070006009301502206000930602080015000050629103009800070000251020530060060210090',
  selectedCell: { row: 3, col: 9 },
  lastSelectedCell: { row: 3, col: 9 },
  selectedValue: undefined,
  cells: [
    { value: 8, fixed: true, pencilValues: [] },          // r1c1
    { value: 3, fixed: true, pencilValues: [] },          // r1c2
    { value: 5, fixed: false, pencilValues: [] },         // r1c3
    { value: 9, fixed: false, pencilValues: [] },         // r1c4
    { value: 7, fixed: true, pencilValues: [] },          // r1c5
    { value: 2, fixed: false, pencilValues: [] },         // r1c6
    { value: 1, fixed: false, pencilValues: [] },         // r1c7
    { value: 4, fixed: false, pencilValues: [] },         // r1c8
    { value: 6, fixed: true, pencilValues: [] },          // r1c9

    { value: 7, fixed: false, pencilValues: [] },         // r2c1
    { value: 4, fixed: false, pencilValues: [] },         // r2c2
    { value: 9, fixed: true, pencilValues: [] },          // r2c3
    { value: 3, fixed: true, pencilValues: [] },          // r2c4
    { value: 6, fixed: false, pencilValues: [] },         // r2c5
    { value: 1, fixed: true, pencilValues: [] },          // r2c6
    { value: 5, fixed: true, pencilValues: [] },          // r2c7
    { value: 8, fixed: false, pencilValues: [] },         // r2c8
    { value: 2, fixed: true, pencilValues: [] },          // r2c9

    { value: 2, fixed: true, pencilValues: [] },          // r3c1
    { value: 1, fixed: false, pencilValues: [] },         // r3c2
    { value: 6, fixed: true, pencilValues: [] },          // r3c3
    { value: 8, fixed: false, pencilValues: [] },         // r3c4
    { value: 4, fixed: false, pencilValues: [] },         // r3c5
    { value: 5, fixed: false, pencilValues: [] },         // r3c6
    { value: 9, fixed: true, pencilValues: [] },          // r3c7
    { value: 3, fixed: true, pencilValues: [] },          // r3c8
    { value: 7, fixed: false, pencilValues: [] },         // r3c9

    { value: 6, fixed: true, pencilValues: [] },          // r4c1
    { value: 9, fixed: false, pencilValues: [] },         // r4c2
    { value: 2, fixed: true, pencilValues: [] },          // r4c3
    { value: 7, fixed: false, pencilValues: [] },         // r4c4
    { value: 8, fixed: true, pencilValues: [] },          // r4c5
    { value: 4, fixed: false, pencilValues: [] },         // r4c6
    { value: 3, fixed: false, pencilValues: [] },         // r4c7
    { value: 1, fixed: true, pencilValues: [] },          // r4c8
    { value: 5, fixed: true, pencilValues: [] },          // r4c9

    { value: 4, fixed: false, pencilValues: [] },         // r5c1
    { value: 8, fixed: false, pencilValues: [] },         // r5c2
    { value: 7, fixed: false, pencilValues: [] },         // r5c3
    { value: 1, fixed: false, pencilValues: [] },         // r5c4
    { value: 5, fixed: true, pencilValues: [] },          // r5c5
    { value: 3, fixed: false, pencilValues: [] },         // r5c6
    { value: 6, fixed: true, pencilValues: [] },          // r5c7
    { value: 2, fixed: true, pencilValues: [] },          // r5c8
    { value: 9, fixed: true, pencilValues: [] },          // r5c9

    { value: 1, fixed: true, pencilValues: [] },          // r6c1
    { value: 5, fixed: false, pencilValues: [] },         // r6c2
    { value: 3, fixed: true, pencilValues: [] },          // r6c3
    { value: 6, fixed: false, pencilValues: [] },         // r6c4
    { value: 2, fixed: false, pencilValues: [] },         // r6c5
    { value: 9, fixed: true, pencilValues: [] },          // r6c1
    { value: 8, fixed: true, pencilValues: [] },          // r6c7
    { value: 7, fixed: false, pencilValues: [] },         // r6c8
    { value: 4, fixed: false, pencilValues: [] },         // r6c9

    { value: 3, fixed: false, pencilValues: [] },         // r7c1
    { value: 7, fixed: true, pencilValues: [] },          // r7c2
    { value: 8, fixed: false, pencilValues: [] },         // r7c3
    { value: 4, fixed: false, pencilValues: [] },         // r7c4
    { value: 9, fixed: false, pencilValues: [] },         // r7c5
    { value: 6, fixed: false, pencilValues: [] },         // r7c6
    { value: 2, fixed: true, pencilValues: [] },          // r7c7
    { value: 5, fixed: true, pencilValues: [] },          // r7c8
    { value: 1, fixed: true, pencilValues: [] },          // r7c9

    { value: 9, fixed: false, pencilValues: [] },         // r8c1
    { value: 2, fixed: true, pencilValues: [] },          // r8c2
    { value: 1, fixed: false, pencilValues: [] },         // r8c3
    { value: 5, fixed: true, pencilValues: [] },          // r8c4
    { value: 3, fixed: true, pencilValues: [] },          // r8c5
    { value: 7, fixed: false, pencilValues: [] },         // r8c6
    { value: 4, fixed: false, pencilValues: [] },         // r8c7
    { value: 6, fixed: true, pencilValues: [] },          // r8c8
    { value: 8, fixed: false, pencilValues: [] },         // r8c9

    { value: 5, fixed: false, pencilValues: [] },         // r9c1
    { value: 6, fixed: true, pencilValues: [] },          // r9c2
    { value: 4, fixed: false, pencilValues: [] },         // r9c3
    { value: 2, fixed: true, pencilValues: [] },          // r9c4
    { value: 1, fixed: true, pencilValues: [] },          // r9c5
    { value: 8, fixed: false, pencilValues: [] },         // r9c6
    { value: 7, fixed: false, pencilValues: [] },         // r9c7
    { value: 9, fixed: true, pencilValues: [] },          // r9c8
    { value: 3, fixed: false, pencilValues: [] },         // r9c9
  ],
};
