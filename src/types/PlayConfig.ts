export type CellControlMode = 'highlight' | 'big' | 'pencil';
export type CellClickMode = 'pencil' | 'select';
export type AdditionalHighlightMode = 'none' | 'singles' | 'binary';

export const CELL_CONTROL_MODE = 'cellControlMode';
export const CELL_CLICK_MODE = 'cellClickMode';
export const ADDITIONAL_HIGHLIGHT_MODE = 'additionalHighlightMode';

export type PlayConfig = {
  cellControlMode: CellControlMode,
  cellClickMode: CellClickMode,
  additionalHighlightMode: AdditionalHighlightMode,
};

export const DefaultPlayConfig: PlayConfig = {
  cellControlMode: 'highlight',
  cellClickMode: 'select',
  additionalHighlightMode: 'none',
};
