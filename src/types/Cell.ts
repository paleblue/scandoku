export type PencilMode = 'corners' | 'center';

export type Cell = {
  value?: number,
  fixed: boolean,
  pencilValues: number[],
  pencilMode?: PencilMode,
};

export const EmptyCell: Cell = {
  value: undefined,
  fixed: false,
  pencilValues: [],
  pencilMode: 'corners',
};

export const SampleCell: Cell = { 
  value: undefined, 
  fixed: false, 
  pencilValues: [2,4,5],
  pencilMode: 'corners',
};

export const SampleFilledCell: Cell = { 
  value: 3, 
  fixed: false, 
  pencilValues: [2,4,5],
  pencilMode: 'corners',
};

export const SampleFixedCell: Cell = { 
  value: 3, 
  fixed: true, 
  pencilValues: [],
  pencilMode: 'corners',
};
