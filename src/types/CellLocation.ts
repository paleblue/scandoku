import { EIGHT } from '@constants';

export type CellLocation = {
  row: number,
  col: number
};

export const DefaultLocation: CellLocation = {
  row: 1,
  col: 1,
};

export const NullLocation: CellLocation = {
  row: -1,
  col: -1,
};

export const cellArrayIndex = (rowOrCell: number | CellLocation, col?: number): number => {
  if (typeof rowOrCell === typeof (DefaultLocation)) {
    return cellArrayIndex((rowOrCell as CellLocation).row, (rowOrCell as CellLocation).col);
  } else if (col) {
    return (((rowOrCell as number)-1)*9) + (col-1);
  } else {
    // shouldn't be here
    return rowOrCell as number;
  }
};

export const cellLocationFromIndex = (n: number): CellLocation => {
  return {
    row: Math.floor(n/9)+1,
    col: Math.floor(n%9)+1,
  };
};

export const cellBox = (rowOrCell: number | CellLocation, col?: number): number => {
  if (typeof rowOrCell === typeof (DefaultLocation)) {
    return cellBox((rowOrCell as CellLocation).row, (rowOrCell as CellLocation).col);
  } else if (col) {
    return (Math.floor(((rowOrCell as number)-1)/3) * 3) + (Math.floor((col-1)/3)) + 1;
  } else {
    // shouldn't be here
    return 0;
  }
};

export const cellBoxByIndex = (n: number): number => {
  return cellBox(cellLocationFromIndex(n));
};

export const indexesOfRow = (row: number): number[] => {
  return EIGHT.map(n => ((row-1) * 9) + n);
};

export const indexesOfColumn = (col: number): number[] => {
  return EIGHT.map(n => (n * 9) + (col - 1));
};

export const indexesOfBox = (boxnum: number): number[] => {
  const boxRow = Math.floor((boxnum-1)/3);
  const boxCol = (boxnum - 1) - (boxRow * 3);
  const rowOffset = boxRow * 3;
  const colOffset = boxCol * 3;
  return EIGHT.map(n => (colOffset + (n % 3)) + (((rowOffset + Math.floor(n/3))*9)));
};

export const isNullLocation = (c?: CellLocation): boolean => {
  if (c) {
    return c.row === NullLocation.row || c.col === NullLocation.col;
  } else {
    // undefined location same as null
    return true;
  }
};
