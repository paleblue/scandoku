import { CellLocation } from '@types';

export type Highlights = {
  valueHighlight: number | undefined,
  primaryHighlight: CellLocation | undefined,
  secondaryHighlights: CellLocation[],
  multiSelectHighlights: CellLocation[],
};

export const EmptyHighlights = {
  valueHighlight: undefined,
  primaryHighlight: undefined,
  secondaryHighlights: [],
  multiSelectHighlights: [],
};
