import { BaseCells } from '@constants';

export type PuzzleCell = { 
  value: number,index: number,
  fixed: boolean,
  row: number,
  col: number,
  box: number,
  candidates: number[],
  peers: number[],
};

export type SolutionResult = string | 'invalid' | 'unsolved' | 'no solution' | 'multiple solutions';

export type SolutionStatistics = {
  numNakedSingle: number,
  timeNakedSingle: number,
  numHiddenSingle: number,
  timeHiddenSingle: number,
  numNakedDouble: number,
  timeNakedDouble: number,
  numHiddenDouble: number,
  timeHiddenDouble: number,
  numNakedTriple: number,
  timeNakedTriple: number,
  numHiddenTriple: number,
  timeHiddenTriple: number,
  numPointingPairs: number,
  timePointingPairs: number,
  numBoxLineReduction: number,
  timeBoxLineReduction: number,
  numGuess: number,
  timeGuess: number,
};
export type SolutionOutput = {
  solution: SolutionResult,
  multipleSolutions?: string[],
  stepCount: number,
  totalTime: number,
  statistics: SolutionStatistics,
};
export type SolutionConfig = {
  allLogging: boolean,
  finalLogging: boolean,
  tableLogging: boolean,
  doNakedSingles: boolean,
  doHiddenSingles: boolean,
  doNakedDoubles: boolean,
  doHiddenDoubles: boolean,
  doNakedTriples: boolean,
  doHiddenTriples: boolean,
  doPointingPairs: boolean,
  doBoxLineReduction: boolean,
};
export type CandidateUnitSet = { count: number, cells: number[] };
export type CandidateSet = { count: number, rows: CandidateUnitSet[], cols: CandidateUnitSet[], boxes: CandidateUnitSet[], cells: number[] };
export type SolutionPacket = { cells: PuzzleCell[], candidateTable: CandidateSet[], cellCandidateCounts: number[] };

export const EmptySolutionStatistics: SolutionStatistics = {
  numNakedSingle: 0,
  timeNakedSingle: 0,
  numHiddenSingle: 0,
  timeHiddenSingle: 0,
  numNakedDouble: 0,
  timeNakedDouble: 0,
  numHiddenDouble: 0,
  timeHiddenDouble: 0,
  numNakedTriple: 0,
  timeNakedTriple: 0,
  numHiddenTriple: 0,
  timeHiddenTriple: 0,
  numPointingPairs: 0,
  timePointingPairs: 0,
  numBoxLineReduction: 0,
  timeBoxLineReduction: 0,
  numGuess: 0,
  timeGuess: 0,
};
export const EmptySolutionOutput: SolutionOutput = {
  solution: 'unsolved',
  stepCount: 0,
  totalTime: 0,
  statistics: { ...EmptySolutionStatistics },
};
export const SolutionConfig_NoLogging: SolutionConfig = {
  allLogging: false,
  finalLogging: false,
  tableLogging: false,
  doNakedSingles: true,
  doHiddenSingles: true,
  doNakedDoubles: true,
  doHiddenDoubles: true,
  doNakedTriples: true,
  doHiddenTriples: true,
  doPointingPairs: true,
  doBoxLineReduction: true,
};
export const SolutionConfig_AllLogging: SolutionConfig = {
  allLogging: true,
  finalLogging: true,
  tableLogging: true,
  doNakedSingles: true,
  doHiddenSingles: true,
  doNakedDoubles: true,
  doHiddenDoubles: true,
  doNakedTriples: true,
  doHiddenTriples: true,
  doPointingPairs: true,
  doBoxLineReduction: true,
};
export const SolutionConfig_OnlySingles: SolutionConfig = {
  allLogging: false,
  finalLogging: false,
  tableLogging: false,
  doNakedSingles: true,
  doHiddenSingles: true,
  doNakedDoubles: false,
  doHiddenDoubles: false,
  doNakedTriples: false,
  doHiddenTriples: false,
  doPointingPairs: false,
  doBoxLineReduction: false,
};
export const EmptySolutionPacket = {
  cells: [ ...BaseCells ],
  candidateTable: [],
  cellCandidateCounts: [],
};
