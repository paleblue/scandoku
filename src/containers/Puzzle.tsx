import React, { FC, ReactElement, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Header, Puzzle as PuzzleComponent, PuzzleCellControls } from '@components';
import { SamplePuzzle, NullLocation } from '@types';
import { usePuzzleDispatch } from '@context';
import '@scss/Puzzle.scss';

type PuzzleParams = {
  puzzleDefinition?: string;
};

const Puzzle: FC = ():ReactElement => {
  const dispatch = usePuzzleDispatch();
  const { puzzleDefinition } = useParams<PuzzleParams>();

  useEffect(() => {
    dispatch({ type: 'setPuzzle', cell: NullLocation, payload: puzzleDefinition || SamplePuzzle});
  }, [dispatch, puzzleDefinition]);

  return (
    <>
      <Header />
      <PuzzleComponent />
      <PuzzleCellControls />
    </>
  );
};

export default Puzzle;
