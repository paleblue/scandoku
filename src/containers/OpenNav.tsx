import React, { FC, ReactElement } from 'react';
import { OpenNavButton, Header } from '@components';
import { useHistory } from 'react-router-dom';
import '@scss/Puzzle.scss';

const OpenNav: FC = ():ReactElement => {
  const history = useHistory();

  return (
    <>
      <Header hideNav={true} />

      <OpenNavButton onClick={() => { history.push('/puzzle'); }}>Easy</OpenNavButton>
      <OpenNavButton onClick={() => { console.log('medium'); }}>Medium</OpenNavButton>
      <OpenNavButton onClick={() => { console.log('hard'); }}>Hard</OpenNavButton>
      <OpenNavButton onClick={() => { console.log('extreme'); }}>Extreme</OpenNavButton>
      <hr style={{ width: '100%' }} />
      <OpenNavButton onClick={() => { console.log('load'); }}>Load Previous</OpenNavButton>
      <OpenNavButton onClick={() => { console.log('create'); }}>Create Puzzle</OpenNavButton>
    </>
  );
};

export default OpenNav;
