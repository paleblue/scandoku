import { Puzzle, LongSample, SamplePuzzle, FilledValuePuzzle, SolutionConfig, SolutionConfig_NoLogging, SolutionOutput, EmptySolutionOutput } from '@types';
import { Atlanta, Boston, Chicago, Eastlake, Fresno } from '@solvers';

const { performance } = require('perf_hooks');

test('can read a puzzle from long form', () => {
  const puzzle: Puzzle = Puzzle.parseString(LongSample);
  expect(puzzle.cells[0].value).toBe(1);
  expect(puzzle.cells[0].fixed).toBe(true);
  expect(puzzle.cells[1].value).toBeUndefined();
  expect(puzzle.cells[1].fixed).toBe(false);
  expect(puzzle.cells[9].value).toBeUndefined();
  expect(puzzle.cells[9].fixed).toBe(false);
  expect(puzzle.cells[10].value).toBe(2);
  expect(puzzle.cells[10].fixed).toBe(true);
  expect(puzzle.cells[11].value).toBeUndefined();
  expect(puzzle.cells[11].fixed).toBe(false);
  expect(puzzle.cells[80].value).toBe(9);
  expect(puzzle.cells[80].fixed).toBe(true);
});

test('can return a puzzle to long form', () => {
  const puzzle: Puzzle = Puzzle.parseString(LongSample);
  const longForm: string = Puzzle.toLongForm(puzzle);
  expect(longForm).toBe(LongSample);
});

test('a puzzle with updated non fixed cell will have the same long form', () => {
  const puzzle: Puzzle = Puzzle.parseString(LongSample);
  puzzle.cells[1] = { value: 6, fixed: false, pencilValues: [] };
  const longForm: string = Puzzle.toLongForm(puzzle);
  expect(longForm).toBe(LongSample);
});

test('a puzzle with updated fixed cell will have a different long form', () => {
  const puzzle: Puzzle = Puzzle.parseString(LongSample);
  puzzle.cells[1] = { value: 6, fixed: true, pencilValues: [] };
  const longForm: string = Puzzle.toLongForm(puzzle);
  expect(longForm).not.toBe(LongSample);
});

test('puzzles id', () => {
  const p: Puzzle = SamplePuzzle;
  const s: string = Puzzle.toLongForm(p);
  expect(p.id).toBe(s);
  const p2: Puzzle = FilledValuePuzzle;
  const s2: string = Puzzle.toLongForm(p2);
  expect(p2.id).toBe(s2);
});

test('puzzle short form', () => {
  const p: Puzzle = SamplePuzzle;
  const l: string = Puzzle.toLongForm(p);
  const s: string = Puzzle.toShortForm(p);
  expect(s).toBe('yJto1TC9MQ6yWQg3aTFSJpNigVVikTmHJRJTZiGQ');

  const p2: Puzzle = Puzzle.parseString(s);
  const l2: string = Puzzle.toLongForm(p2);
  expect(l2).toBe(l);
});

test('puzzle validity', () => {
  expect(Puzzle.isValid(SamplePuzzle)).toBe(true);
  expect(Puzzle.isValid(LongSample)).toBe(true);
  expect(Puzzle.isValid('000000000000000000000000000000000000000000000000000000000000000000000000000000000')).toBe(true);
  expect(Puzzle.isValid('110000000000000000000000000000000000000000000000000000000000000000000000000000000')).not.toBe(true);
  expect(Puzzle.isValid('035972146749361582216845937692784315487153629153629874378496251921537468564218093')).toBe(true);
  expect(Puzzle.isValid('535972146749361582216845937692784315487153629153629874378496251921537468564218093')).not.toBe(true);
});

describe.skip('puzzle solving', () => {

  const ps = [
    '830070006749361582216805937692784315487153629153629874378096251921537468564218093', // 0 - nearly solved
    '635010402901030075000495006100640000067800013309000620090004250004057900016002000', // 1
    '000000012000035000000600070700000300000400800100000000000120000080000040050000600', // 2
    '080700000000000402000000100000042000030000010000060000304000500000800070600100000', // 3 - https://www.free-sudoku.com/sudoku.php?dchoix=evil
    '000000012000035000000600070700000300000400800100000000000120000080000040050000600', // 4 - 17 clues #3
    '000000018320000000400000000008051000040000300000070000706000090000300700000200000', // 5 - 17 clues problematic 1
    '530070000600195000098000060800060003400803001700020006060000280000419005000080079', // 6 - p2
    '000102000008060705090000080400010000080304060300020800060005470005070609070000050', // 7 - p3
    '635010402901030075000495006100640000067800013309000620090004250004057900016002000', // 8 - p4
    '000000000001030000060508300009010530020800076006000010002100003000485090000070800', // 9 - 2016 The Times sudoku grand finale 1 https://www.youtube.com/watch?v=IGxTs6bkgw4
    '000000000008507090010006032040000200000020008023008970000401000071009005004050010', // 10 -2016 The Times sudoku grand finale 3 https://www.youtube.com/watch?v=JgwManX_N6c
    '000006000000000983500290000050082067000500010090073058600710000000000896000008000', // 11 -2016 The Times sudoku grand finale 4 https://www.youtube.com/watch?v=9hodp1O0_bs
    '295743861431865900876192543387459216612387495549216738763534189928671354154938600', // 12 -expect 'invalid'
    '295743861431865900876192543387459216612387495549216738763534189928671354154938',    // 13 -expect 'invalid'
    '286159743357648219419700568821965437693874125745300896568200974134597682972486351', // 14 -expect multiple solutions
    '830470006749361582216805937692784315487153629153629874378096251921537468564218093', // 15 -expect no solution -- some cells have no candidates
    '830470006749361582216805037692784315487153629153629874378006251921037468564218003', // 16 -expect no solution -- all cells have candidates, but no result
    '300009000000001020056002790003000048000040107009000000000000000080360200500820400', // 17 -p5
    '000000512000035900000600070700000300000400800100000000000120000080000040050000600', // 18 -naked single test
    '670032018320000070480700032938051027047020300062073800706000293204300780803207100', // 19 -naked triple test
    '900040000000600031020000090000700020002935600070002000060000073510009000000080009', // 20 -Y-wing test
    '000406000012030500000000070800004007060700050500080009003000020000090010000305000', // 21 -Shye Kingda Ka https://www.youtube.com/watch?v=z3IAgDi6Ves
    '900063000001200500702000030000306050003407600070508000080000105007001200000840009', // 22 -jovial Negativity https://app.crackingthecryptic.com/sudoku/mdtTb9qmhp
    '400701003005000200060003040078600900000050000004002180010800020002000300800205004', // 23 -diabolical example from https://www.sudokuwiki.org/Sudoku_Creation_and_Grading.pdf
    '017903600000080000900000507072010430000402070064370250701000065000030000005601720', // 24 -pointing pairs example from https://www.sudokuwiki.org/Intersection_Removal
    '016007803090800000870001060048000300650009082039000650060900020080002936924600510', // 25 -box/line reduction example from https://www.sudokuwiki.org/Intersection_Removal
  ];
  const ss = [
    '835972146749361582216845937692784315487153629153629874378496251921537468564218793',
    '635718492941236875278495136152643789467829513389571624793164258824357961516982347',
    '673894512912735486845612973798261354526473891134589267469128735287356149351947628',
    '481725369793618452562493187157942836236587914849361725314276598925834671678159243',
    '673894512912735486845612973798261354526473891134589267469128735287356149351947628',
    '679532418325184976481769532938451627147628359562973841716845293294316785853297164',
    '534678912672195348198342567859761423426853791713924856961537284287419635345286179',
    '547182396218963745693547281429618537781354962356729814162895473835471629974236158',
    '635718492941236875278495136152643789467829513389571624793164258824357961516982347',
    '398621457251734689467598321749216538125843976836957214582169743673485192914372865',
    '739214856268537194415986732847195263196723548523648971652471389371869425984352617',
    '149836725267451983583297641351982467872564319496173258628719534714325896935648172',
    'invalid',
    'invalid',
    'multiple solutions',
    'no solution',
    'no solution',
    '328679514497531826156482793713256948265948137849713652672194385984365271531827469',
    'no solution',
    '679532418325184976481769532938451627147628359562973841716845293294316785853297164',
    '931247586754698231628153794195764328482935617376812945869521473513479862247386159',
    '935476281712938546486152973829564137364719852571283469693841725258697314147325698',
    '954163872831274596762985431148326957523497618679518324286739145497651283315842769', 
    '489721563135486279267593841378614952621958437954372186713849625542167398896235714',
    '417953682256187943983246517872519436539462871164378259791824365628735194345691728',
    '416527893592836147873491265148265379657319482239784651361958724785142936924673518',
  ];

  test('hard puzzle solver', async () => {
    console.time('puzzle solver');
    const startTime = performance.now();
    const repCount: number = 100;
    const puzzleIndex = 25;
    // naked triples: puzzle 3, 1000 runs -- 17.7ms without, 24ms with / 47ms with | puzzle 11, 1000 runs -- 1.44ms without, 1.75ms with
    // naked triples: puzzle 19, 1000 runs -- 1.15ms without 4.4ms with
    // all naked/hidden: puzzle 19, 100 runs -- 2.76ms without 14.0ms with
    // all naked/hidden: puzzle 3, 100 runs -- 24.76ms without 101.9ms with
    // all naked/hidden: puzzle 11, 100 runs -- 1.86ms without 4.39ms with
    // all naked/hidden: puzzle 9, 100 runs -- 1.87ms without 3.77ms with
    // all naked/hidden: puzzle 24, 100 runs -- 9.77ms without 46.69ms with
    // all naked/hidden: puzzle 25, 100 runs -- 2.95ms without 15.62ms with
    const solConfig: SolutionConfig = {
      ...SolutionConfig_NoLogging,
      allLogging: false,
      finalLogging: (repCount === 1),
      tableLogging: (repCount === 1),
      // doNakedTriples: false,
      // doHiddenTriples: false,
      // doNakedDoubles: false,
      // doHiddenDoubles: false,
      // doPointingPairs: false,
      // doBoxLineReduction: false,
    };
    let s: SolutionOutput = { ...EmptySolutionOutput };
    for (let j = 0; j < repCount; j++) {
      const solver = new Fresno();
      s = await solver.getSolution(ps[puzzleIndex], solConfig);
    }
    expect(s.solution).toBe(ss[puzzleIndex]);
    if (repCount > 1) {
      console.log(`hard puzzle solve average: ${(performance.now()-startTime)/repCount}`);
    }
    console.timeEnd('puzzle solver');
  });

  test.skip('multi puzzle solver', async () => {
    console.time('puzzle solver');
    const indexes = Array(ps.length).fill(0).map((_c, i) => i);
    const solver = new Fresno();
    const repCount = 1;
    for (let j = 0; j < repCount; j++) {
      for (let i = 0; i < indexes.length; i++) {
        console.log(`PUZZLE ${i}`);
        expect((await solver.getSolution(ps[indexes[i]], { ...SolutionConfig_NoLogging, finalLogging: true })).solution).toBe(ss[indexes[i]]);
      }
    }
    console.timeEnd('puzzle solver');
  });

  describe.skip('puzzle solve time', () => {
    const numRuns = 1;
    const solver = new Fresno();
    const solFunction: (puzzle: string, solutionConfig: SolutionConfig) => Promise<SolutionOutput> = solver.getSolution;

    test('nearly solved', async () => {
      const nr = numRuns;

      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[0], SolutionConfig_NoLogging)).solution).toBe(ss[0]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/nr).toBeLessThan(5.0);
    });
    describe('17 value samples', () => {
      test('"evil"', async () => {
        const startTime = performance.now();
        for (let i = 0; i < numRuns; i++) {
          expect((await solFunction(ps[3], SolutionConfig_NoLogging)).solution).toBe(ss[3]);
        }
        const endTime = performance.now();
        const elapsedTime = endTime - startTime;
        expect(elapsedTime/numRuns).toBeLessThan(100);
      });
      test('all 17 clues row 3', async () => {
        const startTime = performance.now();
        for (let i = 0; i < numRuns; i++) {
          expect((await solFunction(ps[4], SolutionConfig_NoLogging)).solution).toBe(ss[4]);
        }
        const endTime = performance.now();
        const elapsedTime = endTime - startTime;
        expect(elapsedTime/numRuns).toBeLessThan(5);
      });
      test('all 17 clues problematic 1', async () => {
        const startTime = performance.now();
        for (let i = 0; i < numRuns; i++) {
          expect((await solFunction(ps[5], SolutionConfig_NoLogging)).solution).toBe(ss[5]);
        }
        const endTime = performance.now();
        const elapsedTime = endTime - startTime;
        expect(elapsedTime/numRuns).toBeLessThan(15);
      });
    });
    test('p2', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[6], SolutionConfig_NoLogging)).solution).toBe(ss[6]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(8);
    });
    test('p3', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[7], SolutionConfig_NoLogging)).solution).toBe(ss[7]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(10);
    });
    test('p4', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[8], SolutionConfig_NoLogging)).solution).toBe(ss[8]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(2.5);
    });
    test('2016 The Times sudoku grand final 1', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[9], SolutionConfig_NoLogging)).solution).toBe(ss[9]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(3);
    });
    test('2016 The Times sudoku grand final 3', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[10], SolutionConfig_NoLogging)).solution).toBe(ss[10]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(5);
    });
    test('2016 The Times sudoku grand final 4', async () => {
      const startTime = performance.now();
      for (let i = 0; i < numRuns; i++) {
        expect((await solFunction(ps[11], SolutionConfig_NoLogging)).solution).toBe(ss[11]);
      }
      const endTime = performance.now();
      const elapsedTime = endTime - startTime;
      expect(elapsedTime/numRuns).toBeLessThan(4);
    });
  });

  test.skip('long solve puzzle time', async () => {
    const fs = require('fs');
    // const allPuzzles = fs.readFileSync('./src/utilities/all_17_clue_sudokus.txt').toString().split('\n');
    // const allPuzzles = fs.readFileSync('./src/utilities/5000_17_clue_sudokus.txt').toString().split('\n');
    // const allPuzzles = fs.readFileSync('./src/utilities/10_17_clue_sudokus.txt').toString().split('\n');
    const allPuzzles = fs.readFileSync('./src/utilities/250_17_clue_sudokus.txt').toString().split('\n');
    const startTime = performance.now();
    const solver = new Fresno();
    const sols: string[] = [];
    let pcount: number = 0;
    for (let i=0; i < allPuzzles.length; i++) {
      const p = allPuzzles[i];
      if (p.length === 81) {
        pcount++;
        sols.push((await solver.getSolution(p, SolutionConfig_NoLogging)).solution);
      }
    };
    const endTime = performance.now();
    const elapsedTime = endTime - startTime;
    console.log(`elapsedTime of ${pcount} puzzles`, elapsedTime, 'ms\n', sols);
    expect(elapsedTime).toBeLessThan(10000);
  });
});

// test.skip('puzzle creation', async () => {
  // const p1 = await Puzzle.createValidPuzzle();
  // expect(Puzzle.isSolved(p1)).toBe(true);
// });
